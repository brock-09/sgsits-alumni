import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Text, Platform } from 'react-native';
import { Root } from 'native-base';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './src/redux/store/store';
import Main from './src/navigation';
import InternetConnection from './src/components/InternetConnection';

const fetchFonts = () => {
    return Font.loadAsync({
        'Roboto_light': require('././assets/Fonts/Roboto-Light.ttf'),
        'Roboto_regular': require('././assets/Fonts/Roboto-Regular.ttf'),
        'Roboto_medium': require('././assets/Fonts/Roboto-Medium.ttf'),
        'Roboto_bold': require('././assets/Fonts/Roboto-Bold.ttf')
    });
};

export default function App() {
    console.disableYellowBox = true;
    const [ fontLoad, setFontLoad ] = useState(false);

    if (!fontLoad) {
        return <AppLoading startAsync={ fetchFonts } onFinish={ () => setFontLoad(true) } />
    }

    if (Platform.OS === 'web') {
        return (
            <Text>Not Compatible with WEB</Text>
        );
    };
    return (
        <Provider store={ store } >
            <PersistGate persistor={ persistor } loading={ null } >
                <Root>
                    <Main />
                    <StatusBar style='auto' />
                    <InternetConnection />
                </Root>
            </PersistGate>
        </Provider>
    );
};