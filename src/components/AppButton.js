import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Text } from 'native-base';
import colors from '../constants/colors';
import textStyles from '../constants/textStyles';

const AppButton = props => {
  return (
    <Button title={ props.title } style={ { ...styles.Button, ...props.style } } { ...props }>
      <Text style={ { ...textStyles.bodyText, ...styles.Text, ...props.style } }>{ (props.children) || (props.title) }</Text>
    </Button>
  );
};

const styles = StyleSheet.create({

  Button: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 3,
    shadowOpacity: 0.26,
    elevation: 4,
    borderRadius: 5,

    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center'
  },

  Text: {
    color: colors.base,
  },

});

export default AppButton;