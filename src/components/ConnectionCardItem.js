import React from 'react';
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { Card, Text } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import textStyles from '../constants/textStyles';
import AppButton from './AppButton';
import LoadingScreen from './LoadingScreen';
import { connect } from 'react-redux';
import images from '../constants/images';

const ConnectionCardItem = props => {
    const navigation = useNavigation();
    //Fetching Data as an Prop to Display in Card
    const result = props.data;
    const accountDetails = props.Auths[0].name;

    if (result === undefined || accountDetails === undefined) {
        return <LoadingScreen />
    }

    //Changing viewId
    const viewId = result.connectionId !== accountDetails.accountId ? result.connectionId : result.accountId;

    return (
        <Card style={styles.Container}>
            <View style={styles.imageContainer}>
                <Image
                    source={{
                        uri: result.profilePic != null
                            ? result.profilePic
                            : images.DEFAULT_PROFILE
                    }}
                    style={styles.Image} />
            </View>
            <View style={styles.Container}>
                <View style={styles.TitleContainer}><Text style={{ ...textStyles.regularText, ...styles.text }} numberOfLines={3}>{result.firstName + " " + result.lastName}</Text></View>
                <View style={styles.TitleContainer}><Text style={textStyles.bodyText}>{result.batch}</Text></View>
                <View style={styles.TitleContainer}><Text style={textStyles.bodyText}>{result.latestJobTitle !== null ? result.latestJobTitle : null}</Text></View>
                <View style={styles.ButtonContainer}><AppButton small onPress={() => navigation.push('UserProfile', { id: viewId })}>Profile</AppButton></View>
            </View>
        </Card>
    );
};

const styles = StyleSheet.create({

    Container: {
        flex: 1,
        marginVertical: 10,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 158
    },

    imageContainer: {
        marginTop: 5,
        height: 100,
        width: 100,
    },

    Image: {
        width: '100%',
        height: '100%',
        borderRadius: Math.floor(Dimensions.get('window').width * 0.7 / 2),
        overflow: 'hidden',
    },

    TitleContainer: {
        justifyContent: 'center',
    },

    text: {
        fontSize: 16,
        textAlign: 'center'
    },

    ButtonContainer: {
        marginTop: 10,
        justifyContent: 'space-between',
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(ConnectionCardItem);