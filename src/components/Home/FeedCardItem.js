import React from 'react';
import { StyleSheet } from 'react-native';
import { Text, Left, Thumbnail, Body, List, ListItem } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import moment from 'moment';
import LoadingScreen from '../LoadingScreen';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import images from '../../constants/images';

const FeedCardItem = props => {
    const navigation = useNavigation();
    //Fetching Data as an Prop to Display in Card
    const result = props.data;
    const accountDetails = props.Auths[ 0 ].name;
    const viewId = result.connectionId !== accountDetails.accountId ? result.connectionId : result.accountId;

    if (result === undefined) {
        return <LoadingScreen />
    }

    return (
        <List>
            <ListItem avatar >
                <Left>
                    <Thumbnail source={ {
                        uri: result.profilePic != null
                            ? result.profilePic
                            : images.DEFAULT_PROFILE
                    } } />
                </Left>
                <Body>
                    <Text>
                        { result.activityType === 'connection'
                            ? <Text style={ { ...textStyles.bodyText, ...styles.text } } onPress={ () => navigation.navigate('UserProfile', { id: viewId }) }>
                                { result.firstName + " " + result.lastName }
                            </Text>
                            : <Text style={ { ...textStyles.bodyText, ...styles.text } } onPress={ () => navigation.navigate('UserProfile', { id: result.accountId }) }>
                                { result.firstName + " " + result.lastName }
                            </Text> }

                        { result.activityType === 'connection'
                            ? <Text note> became friends with <Text style={ { ...textStyles.bodyText, ...styles.text } }>You</Text></Text>
                            : <Text note> posted a
                                 <Text style={ { ...textStyles.bodyText, ...styles.text } }
                                    onPress={ () => navigation.navigate('ReadJob', { FeedCardItem: result }) }> Job</Text> </Text>
                        }</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.dateText } }>{ moment(result.createdAt).format('MMM Do YYYY') }</Text>
                </Body>
            </ListItem>
        </List>

    )
};

const styles = StyleSheet.create({

    TextContainer: {
        marginHorizontal: 20,
    },

    text: {
        fontSize: 16,
        color: colors.primary
    },

    dateText: {
        fontSize: 12,
        alignSelf: 'flex-end'
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(FeedCardItem);