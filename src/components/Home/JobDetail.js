import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Alert } from 'react-native';
import { Form, Item, Label, Picker, ListItem, CheckBox, Container, Content, Button } from 'native-base';
import { TextInput } from 'react-native-paper';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import colors from '../../constants/colors';
import textStyles from '../../constants/textStyles';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import LoadingScreen from '../LoadingScreen';
import api from '../../constants/api';

const JobDetail = props => {

    const [ data, setData ] = useState()
    const [ createJob, setCreateJob ] = useState()
    const [ loading, setLoading ] = useState(false)
    const initialValues = {
        jobTitle: '',
        experience: '0-1 years',
        designation: '',
        jobLocation: '',
        jobShortDescription: '',
        jobLongDescription: '',
        jobRequiredSkills: '',
        packages: '',
        vacancy: '',
        fullTime: true,
        partTime: false,
        contract: false
    }

    const validationSchema = Yup.object({
        jobTitle: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .min(3, 'Job Title min 3 character')
            .required('Required'),
        designation: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .required('Required'),
        jobLocation: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .required('Required'),
        jobShortDescription: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .min(3, 'should be 3 character')
            .required('Required'),
        jobRequiredSkills: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .required('Required'),
        experience: Yup.string().required('Required'),
        packages: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .required('Required'),
        vacancy: Yup.number()
            .positive('Vacancy cannot be 0.')
            .max(1000)
            .required('Required'),

    })

    useEffect(() => {
        setData(props.route.params.CreateJobPost)
    })

    const onSubmit = values => {
        setLoading(true)
        const accountDetails = props.Auths[ 0 ].name;
        let params = {
            url: api.BASE + api.CREATE_JOB_POST,
            configuration: {
                headers: {
                    'Authorization': "Bearer " + accountDetails.token
                }
            },
            data: {
                id: accountDetails.accountId,
                ...formik.values,
                ...data,
                jobPostedBy: accountDetails.accountId,
            }
        }

        axios.post(params.url, params.data, params.configuration)
            .then(response => {
                setLoading(false)
                const result = response.data.data[ 0 ];
                setCreateJob(result);
                return props.navigation.replace('Home');
            })
            .catch((error) => {
                console.log('JobDetail', error);
            });
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })

    if (loading) {
        return <LoadingScreen />
    }

    return (
        <Container>
            <Content style={ styles.container }>
                <Form >
                    <TextInput
                        label='Job Title'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('jobTitle') }
                        onChangeText={ formik.handleChange('jobTitle') }
                        value={ formik.values.jobTitle }
                        error={ formik.touched.jobTitle && formik.errors.jobTitle ? true : false } />
                    { formik.touched.jobTitle && formik.errors.jobTitle ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.jobTitle }</Text> : null }

                    <Item last>
                        <Label style={ textStyles.bodyText } >Experience :</Label>
                        <Picker
                            mode='dropdown'
                            onBlur={ formik.handleBlur('experience') }
                            selectedValue={ formik.values.experience }
                            onValueChange={ value => { formik.setFieldValue('experience', value) } }>
                            <Picker.Item label='0-1 years' value='0-1 years' />
                            <Picker.Item label='1-2 years' value='1-2 years' />
                            <Picker.Item label='2-3 years' value='2-3 years' />
                            <Picker.Item label='3-4 years' value='3-4 years' />
                            <Picker.Item label='4-6 years' value='4-6 years' />
                            <Picker.Item label='6-8 years' value='6-8 years' />
                            <Picker.Item label='8-10 years' value='8-10 years' />
                            <Picker.Item label='10-12 years' value='10-12 years' />
                            <Picker.Item label='12-14 years' value='12-14 years' />
                            <Picker.Item label='14-15 years' value='14-15 years' />
                            <Picker.Item label=' More than 15 years' value='More than 15 years' />
                        </Picker>
                        { formik.touched.experience && formik.errors.experience ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.experience }</Text> : null }
                    </Item>

                    <TextInput
                        label='Job Designation'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('designation') }
                        onChangeText={ formik.handleChange('designation') }
                        value={ formik.values.designation }
                        error={ formik.touched.designation && formik.errors.designation ? true : false } />
                    { formik.touched.designation && formik.errors.designation ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.designation }</Text> : null }

                    <TextInput
                        label='Job Location'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('jobLocation') }
                        onChangeText={ formik.handleChange('jobLocation') }
                        value={ formik.values.jobLocation }
                        error={ formik.touched.jobLocation && formik.errors.jobLocation ? true : false } />
                    { formik.touched.jobLocation && formik.errors.jobLocation ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.jobLocation }</Text> : null }

                    <TextInput
                        label='Job Short Description'
                        mode='flat'
                        multiline={ true }
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('jobShortDescription') }
                        onChangeText={ formik.handleChange('jobShortDescription') }
                        value={ formik.values.jobShortDescription }
                        error={ formik.touched.jobShortDescription && formik.errors.jobShortDescription ? true : false } />
                    { formik.touched.jobShortDescription && formik.errors.jobShortDescription ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.jobShortDescription }</Text> : null }

                    <TextInput
                        label='Job Long Description'
                        mode='flat'
                        multiline={ true }
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('jobLongDescription') }
                        onChangeText={ formik.handleChange('jobLongDescription') }
                        value={ formik.values.jobLongDescription } />


                    <TextInput
                        label='Job Required Skills'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('jobRequiredSkills') }
                        onChangeText={ formik.handleChange('jobRequiredSkills') }
                        value={ formik.values.jobRequiredSkills }
                        error={ formik.touched.jobRequiredSkills && formik.errors.jobRequiredSkills ? true : false } />
                    { formik.touched.jobRequiredSkills && formik.errors.jobRequiredSkills ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.jobRequiredSkills }</Text> : null }

                    <TextInput
                        label='Package'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('packages') }
                        onChangeText={ formik.handleChange('packages') }
                        value={ formik.values.packages }
                        error={ formik.touched.packages && formik.errors.packages ? true : false } />
                    { formik.touched.packages && formik.errors.packages ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.packages }</Text> : null }

                    <TextInput
                        label='Vacancy'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        keyboardType="numeric"
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('vacancy') }
                        onChangeText={ formik.handleChange('vacancy') }
                        value={ formik.values.vacancy }
                        error={ formik.touched.vacancy && formik.errors.vacancy ? true : false } />
                    { formik.touched.vacancy && formik.errors.vacancy ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.vacancy }</Text> : null }

                    <Item last style={ { justifyContent: 'space-around' } }>
                        <Label style={ textStyles.bodyText }>Type:</Label>
                        <ListItem >
                            <CheckBox
                                onPress={ () => formik.setFieldValue('fullTime', formik.values.fullTime ? false : true) }
                                checked={ formik.values.fullTime } color={ colors.primary } />
                            <Text style={ styles.checkboxMargin }>Full Time</Text>
                        </ListItem>
                        <ListItem >
                            <CheckBox
                                onPress={ () => formik.setFieldValue('partTime', formik.values.partTime ? false : true) }
                                checked={ formik.values.partTime } color={ colors.primary } />
                            <Text style={ styles.checkboxMargin }>Part Time</Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox
                                onPress={ () => formik.setFieldValue('contract', formik.values.contract ? false : true) }
                                checked={ formik.values.contract } color={ colors.primary } />
                            <Text style={ styles.checkboxMargin }>Contract</Text>
                        </ListItem>
                    </Item>
                    {
                        formik.values.fullTime || formik.values.partTime || formik.values.contract
                            ? null
                            : <Text style={ { ...textStyles.bodyText, ...styles.errors } }>Please select atleast one Type</Text>
                    }

                    <View style={ styles.buttonContainer }>
                        <AppButton
                            title='Submit'
                            type='submit'
                            disabled={ formik.values.fullTime || formik.values.partTime || formik.values.contract ? false : true }
                            onPress={ formik.handleSubmit }>Add</AppButton>
                        <AppButtonSec title='Cancel' type='reset' onPress={ formik.handleReset }>Cancel</AppButtonSec>
                    </View>
                </Form>
            </Content>
        </Container>
    )
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        marginHorizontal: 15,
    },

    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },

    errors: {
        fontSize: 12,
        color: 'red',
        marginLeft: 15,
        margin: 5
    },

    checkboxMargin: {
        marginHorizontal: 3
    },
})

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(JobDetail)