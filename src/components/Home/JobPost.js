import React, { useState } from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Form, View, Icon, Text, Thumbnail, Item, Picker } from 'native-base';
import { TextInput } from 'react-native-paper';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import * as ImagePicker from 'expo-image-picker';
import countryCodeList from '../../util/countryCodeList';
import AppButton from '../AppButton';
import colors from '../../constants/colors';
import textStyles from '../../constants/textStyles';
import images from '../../constants/images';


const JobPost = props => {
	const [ jobLogoPic, setJobLogoPic ] = useState();
	const [ countryCode, setCountryCode ] = useState('+91');

	const initialValues = {
		jobLogo: '',
		companyName: '',
		companyEmailAddress: '',
		companyAddress: '',
		companyWebsite: '',
		companyContactNo: ''
	}

	const phoneRegExp = /^(0|[1-9][0-9]{9})$/i
	const urlRegExp = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
	const validationSchema = Yup.object({
		jobLogo: Yup.string()
			.required('Job Logo Required'),
		companyName: Yup.string()
			.strict(true)
			.trim('Whitespaces not allowed')
			.min(3, 'First Name min 3 character')
			.required('Required'),
		companyEmailAddress: Yup.string()
			.strict(true)
			.trim('Whitespaces not allowed')
			.email('Invalid Format')
			.required('Required'),
		companyContactNo: Yup.string()
			.matches(phoneRegExp, 'Phone Number Not Valid')
			.min(10, 'Min. characters 10')
			.max(13, 'Max. character 10').required('Required'),
		companyAddress: Yup.string()
			.strict(true)
			.trim('Whitespaces not allowed')
			.min(3, 'Address min 3 character')
			.required('Required'),
		companyWebsite: Yup.string()
			.strict(true)
			.trim('Whitespaces not allowed')
			.matches(urlRegExp, 'Link Not Valid')
			.required('Required'),
	})

	const onSubmit = (values) => {
		let mobile = countryCode + formik.values.companyContactNo;
		const data = {
			...formik.values,
			companyContactNo: mobile
		}
		return props.navigation.navigate('JobDetail', { CreateJobPost: data })
	}

	const pickImage = async () => {
		try {
			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Image,
				allowsEditing: true,
				base64: true,
				aspect: [ 3, 3 ],
				quality: 1,
			});

			if (!result.cancelled) {
				setJobLogoPic(`data:image/jpg;base64,${ result.base64 }`)
				formik.setFieldValue('jobLogo', `data:image/jpg;base64,${ result.base64 }`)
			}
		} catch (error) {
			console.log(error);
		}
	};

	const formik = useFormik({
		initialValues,
		onSubmit,
		validationSchema
	})

	return (
		<ScrollView>
			<View style={ styles.Container } >
				<Form>
					<View style={ styles.imageContainer }>
						<View style={ styles.imageContainer }>
							<Thumbnail square large source={ jobLogoPic != null
								? { uri: jobLogoPic }
								: images.DEFAULT_JOBLOGO } />
							<Icon
								name="pencil"
								type="FontAwesome"
								style={ styles.EditIcon }
								onPress={ () => { pickImage() } } />
						</View>
					</View>
					{ formik.touched.jobLogo && formik.errors.jobLogo
						? <Text style={ { ...textStyles.bodyText, ...styles.errors, alignSelf: 'center' } }>{ formik.errors.jobLogo }</Text> : null }

					<TextInput
						label='Company Name'
						mode='flat'
						theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
						dense={ true }
						autoCapitalize='words'
						returnKeyType='next'
						onBlur={ formik.handleBlur('companyName') }
						onChangeText={ formik.handleChange('companyName') }
						value={ formik.values.companyName }
						error={ formik.touched.companyName && formik.errors.companyName ? true : false } />
					{ formik.touched.companyName && formik.errors.companyName
						? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.companyName }</Text> : null }

					<TextInput
						label='Company Email'
						mode='flat'
						theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
						dense={ true }
						keyboardType='email-address'
						autoCapitalize='none'
						returnKeyType='next'
						autoCompleteType='email'
						textContentType='emailAddress'
						onBlur={ formik.handleBlur('companyEmailAddress') }
						onChangeText={ formik.handleChange('companyEmailAddress') }
						value={ formik.values.companyEmailAddress }
						error={ formik.touched.companyEmailAddress && formik.errors.companyEmailAddress ? true : false } />
					{ formik.touched.companyEmailAddress && formik.errors.companyEmailAddress
						? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.companyEmailAddress }</Text> : null }

					<TextInput
						label='Company Address'
						mode='flat'
						multiline={ true }
						theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
						dense={ true }
						autoCapitalize='words'
						returnKeyType='next'
						onBlur={ formik.handleBlur('companyAddress') }
						onChangeText={ formik.handleChange('companyAddress') }
						value={ formik.values.companyAddress }
						error={ formik.touched.companyAddress && formik.errors.companyAddress ? true : false } />
					{ formik.touched.companyAddress && formik.errors.companyAddress
						? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.companyAddress }</Text> : null }

					<Item last>
						<Picker
							mode="dropdown"
							placeholder="drop"
							style={ { width: '40%' } }
							selectedValue={ countryCode }
							onValueChange={ (value) => value !== '' ? setCountryCode(value) : null }>
							<Picker.Item label="Code" value='' />
							{ countryCodeList !== undefined
								? countryCodeList.map((data, index) => {
									return <Picker.Item key={ index } label={ data.label + ' ' + data.value } value={ data.value } />
								})
								: <Picker.Item label='Loading' value='' /> }
						</Picker>
						<TextInput
							style={ { width: '60%', marginBottom: -2 } }
							label='Mobile No'
							mode='flat'
							underlineColor='transparent'
							theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
							dense={ true }
							keyboardType="numeric"
							returnKeyType='next'
							maxLength={ 10 }
							onBlur={ formik.handleBlur('companyContactNo') }
							onChangeText={ formik.handleChange('companyContactNo') }
							value={ formik.values.companyContactNo }
							error={ formik.touched.companyContactNo && formik.errors.companyContactNo ? true : false } />
					</Item>
					{ formik.touched.companyContactNo && formik.errors.companyContactNo
						? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.companyContactNo }</Text> : null }

					<TextInput
						label='Company Website'
						mode='flat'
						theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
						dense={ true }
						keyboardType='url'
						returnKeyType='next'
						textContentType='URL'
						onBlur={ formik.handleBlur('companyWebsite') }
						onChangeText={ formik.handleChange('companyWebsite') }
						value={ formik.values.companyWebsite }
						error={ formik.touched.companyWebsite && formik.errors.companyWebsite ? true : false } />
					{ formik.touched.companyWebsite && formik.errors.companyWebsite
						? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.companyWebsite }</Text> : null }

					<View style={ styles.buttonContainer }>
						<AppButton onPress={ formik.handleSubmit }>Next</AppButton>
					</View>
				</Form>
			</View>
		</ScrollView>
	)
}

const styles = StyleSheet.create({
	Container: {
		marginVertical: 10,
		marginHorizontal: 15,
	},

	imageContainer: {
		flexDirection: 'row',
		justifyContent: 'center'
	},

	buttonContainer: {
		margin: 10,
		justifyContent: 'space-between',
		alignItems: 'flex-end',
	},

	errors: {
		fontSize: 12,
		color: 'red',
		marginLeft: 15,
		margin: 5
	},

	EditIcon: {
		fontSize: 18,
		padding: 8,
		right: -10,
		bottom: -5,
		borderRadius: 50,
		color: colors.base,
		backgroundColor: colors.primary,
		position: 'absolute',
		justifyContent: 'center',
	},
})
const mapStateToProps = (state) => {
	return {
		Auths: state.AuthReducer.account
	}
}


export default connect(mapStateToProps)(JobPost);