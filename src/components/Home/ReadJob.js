import React, { useEffect, useState } from 'react';
import { View, Text, List, ListItem, Left, Thumbnail, Body, Icon, Right } from 'native-base';
import { StyleSheet, Image, Alert } from 'react-native';
import { axiosall } from '../../util/axiosall';
import moment from 'moment';
import { connect } from 'react-redux';
import LoadingScreen from '../LoadingScreen';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import api from '../../constants/api';
import images from '../../constants/images';

const ReadJob = (props) => {
    const result = props.route.params.FeedCardItem
    const [ getReadJob, setGetReadJob ] = useState()
    const [ loading, setLoading ] = useState(false)

    useEffect(() => {
        setLoading(true)
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: {
                accountId: accountDetails.accountId,
                id: result.id
            }
        };

        axiosall('get', api.READ_JOB_POST, configuration)
            ?.then(response => {
                const data = response.data;
                setGetReadJob(data);
                setLoading(false)
            })
            .catch((error) => {
                console.log(error);
            });
    }, [])

    const DeleteJob = () => {
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: {
                accountId: accountDetails.accountId,
                id: result.id
            }
        };
        console.log(configuration);

        axiosall('delete', api.DELETE_JOB_POST, configuration)
            ?.then(response => {
                return props.navigation.replace('Home')
            })
            .catch((error) => {
                console.log('DELETE Job', error);
            });
    }

    const AlertButton = () => {
        Alert.alert(
            "Delete Job",
            `Are you sure you want to delete the job ${ getReadJob.companyName } ?`,
            [
                {
                    text: "Yes",
                    onPress: () => { DeleteJob() },
                    style: { ...textStyles.bodyText, color: colors.primary }
                },
                { text: "No" }
            ],
            { cancelable: false }
        );
    }

    if (loading || getReadJob === undefined) {
        return <LoadingScreen />
    }

    const ReadCard = () => {
        const accountDetails = props.Auths[ 0 ].name;
        return (
            <View>
                < View style={ styles.jobContainer }>
                    <View >
                        <Image
                            source={ { uri: getReadJob.jobLogo } }
                            style={ styles.photoStyle } />
                    </View>
                    <Body style={ { alignItems: 'flex-start' } }>
                        <Text style={ textStyles.regularText }>{ getReadJob.companyName }</Text>

                        <View style={ styles.Row }>
                            <View style={ styles.IconWidth }>
                                <Icon name='user' type='FontAwesome' style={ styles.IconStyle } />
                            </View>
                            <Text note style={ textStyles.bodyText }>{ getReadJob.designation }{ '\n' }({ getReadJob.vacancy } opening)</Text>
                        </View>
                        <View style={ styles.Row }>
                            <View style={ styles.IconWidth }>
                                <Icon name='briefcase' type='FontAwesome' style={ styles.IconStyle } />
                            </View>
                            <Text note style={ textStyles.bodyText }>{ getReadJob.experience }</Text>
                        </View>
                        <View style={ styles.Row }>
                            <View style={ styles.IconWidth }>
                                <Icon name='rupee' type='FontAwesome' style={ styles.IconStyle } />
                            </View>
                            <Text note style={ textStyles.bodyText }>{ getReadJob.package }</Text>
                        </View>
                        <View style={ styles.Row }>
                            <View style={ styles.IconWidth }>
                                <Icon name='location-pin' type='Entypo' style={ styles.IconStyle } />
                            </View>
                            <Text note style={ textStyles.bodyText }>{ getReadJob.jobLocation }</Text>
                        </View>
                        <View style={ styles.Row }>
                            <View style={ styles.IconWidth }>
                                <Icon name='mail' type='Feather' style={ styles.IconStyle } />
                            </View>
                            <Text note style={ textStyles.bodyText }>{ getReadJob.companyEmailAddress }</Text>
                        </View>
                        <View style={ styles.Row }>
                            <View style={ styles.IconWidth }>
                                <Icon name='globe-asia' type='FontAwesome5' style={ styles.IconStyle } />
                            </View>
                            <Text note style={ textStyles.bodyText }>{ getReadJob.companyWebsite }</Text>
                        </View>
                    </Body>
                </View>
                <View style={ { marginLeft: 25, marginTop: 5 } }>
                    <Text style={ { ...textStyles.bodyText } }>Required Skills: <Text note style={ { ...textStyles.bodyText } }>{ getReadJob.jobRequiredSkills }</Text></Text>
                    <Text style={ { ...textStyles.bodyText } }>{ getReadJob.jobShortDescription }</Text>
                </View>
                { accountDetails.accountId === getReadJob.jobPostedBy
                    ? <View style={ styles.buttonContainer }>

                        <AppButtonSec onPress={ () => { AlertButton() } }>Delete Job Post</AppButtonSec>
                    </View>
                    : null }
            </View>);
    }

    return (
        <View style={ styles.container }>
            <List>
                <ListItem thumbnail>
                    <Left>
                        <Thumbnail circular source={ {
                            uri: result.profilePic != null
                                ? result.profilePic
                                : images.DEFAULT_PROFILE
                        } } />
                    </Left>
                    <Body>
                        <Text style={ { ...textStyles.regularText, color: colors.primary } }>
                            { result.firstName + ' ' + result.lastName }
                        </Text>
                        <Text note style={ textStyles.bodyText }>posted a Job</Text>
                    </Body>
                    <Right>
                        <Text note style={ textStyles.bodyText }>
                            { moment(result.createdAt).format('MMM Do YYYY') }
                        </Text>
                    </Right>
                </ListItem>
            </List>
            <ReadCard />
        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: colors.base,
    },

    jobContainer: {
        flexDirection: 'row',
        paddingTop: 20,
    },

    photoStyle: {
        width: 100,
        height: 100,
        marginHorizontal: 15,
        overflow: 'hidden'
    },

    Row: {
        flexDirection: 'row'
    },

    IconWidth: {
        width: 25
    },

    MarginStart: {
        marginStart: 3
    },

    IconStyle: {
        fontSize: 12,
        margin: 5,
        color: colors.primary,
        alignSelf: 'center'
    },

    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
})
const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}
export default connect(mapStateToProps)(ReadJob)