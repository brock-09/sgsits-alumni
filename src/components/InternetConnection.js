import React, { useState, useEffect } from 'react'
import { Footer, FooterTab, Text, View } from 'native-base';
import { StyleSheet } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import colors from '../constants/colors';
import textStyles from '../constants/textStyles';

const InternetConnection = () => {
    const [ checkConnection, setCheckConnection ] = useState('')

    useEffect(() => {
        NetInfo.addEventListener(state => {
            return (
                setCheckConnection(state.isConnected)
            );
        });
    })
    return (
        <View>
            { checkConnection
                ? null
                :
                <Footer>
                    <FooterTab style={ styles.FooterContainer }>
                        <Text style={ { ...textStyles.bodyText, color: colors.base, fontSize: 16 } }>{ checkConnection ? null : 'No Internet Connection !' }</Text>
                    </FooterTab>
                </Footer>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    FooterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
    }
})

export default InternetConnection;

