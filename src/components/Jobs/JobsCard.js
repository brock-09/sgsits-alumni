import React from 'react';
import { StyleSheet, Image, Linking } from 'react-native';
import { Card, Text, CardItem, View } from 'native-base';
import TagButton from '../TagButton';
import LoadingScreen from '../LoadingScreen';
import images from '../../constants/images';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';

const JobsCard = props => {

    const result = props.data;

    if (result === undefined) {
        return <LoadingScreen />
    }

    return (
        <Card style={ styles.card }>
            <CardItem style={ styles.imgContainer }>
                <Image source={ result.jobLogo !== null
                    ? { uri: result.jobLogo }
                    : images.DEFAULT_JOBLOGO
                }
                    style={ styles.img } />
            </CardItem>

            <CardItem bordered style={ styles.textContainer }>
                <Text style={ { ...textStyles.regularText, ...styles.HeadingText } }>{ result.companyName }</Text>
            </CardItem>

            <CardItem bordered style={ styles.textContainer }>
                <Text style={ { ...textStyles.bodyText, ...styles.HeadingText } } numberOfLines={ 10 }>{ result.jobLongDescription }</Text>
            </CardItem>


            <View style={ styles.HtagContainer }>
                <TagButton>{ result.jobRequiredSkills }</TagButton>
                <TagButton>{ result.designation }</TagButton>
            </View>
            <View style={ styles.tagContainer }>
                <TagButton>{ result.vacancy }</TagButton>
                <TagButton>{ result.package }</TagButton>
            </View>
            <View style={ styles.tagContainer }>
                <TagButton>{ result.experience }</TagButton>
                <TagButton>{ result.jobLocation }</TagButton>
            </View>

            <CardItem bordered style={ styles.CardItemtagContainer } />

            <View style={ styles.tagContainer }>
                <TagButton onPress={ () => Linking.openURL(result.companyWebsite) }>{ result.companyWebsite }</TagButton>
                <TagButton onPress={ () => Linking.openURL('mailto:' + result.companyEmailAddress) }>{ result.companyEmailAddress }</TagButton>
            </View>
        </Card>
    );
};

const styles = StyleSheet.create({
    card: {
        padding: 10,
        backgroundColor: colors.base,
        borderRadius: 10
    },

    imgContainer: {
        height: 100,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    HeadingText: {
        textAlign: 'center',
    },

    img: {
        width: 100,
        height: 100,
    },

    HtagContainer: {
        marginTop: 30,
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    tagContainer: {
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    }

});

export default JobsCard;