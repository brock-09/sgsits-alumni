import React, { useEffect, useState, useCallback } from 'react';
import { Thumbnail, Icon } from 'native-base';
import { connect } from 'react-redux';
import axios from 'axios';
import { axiosall } from '../../util/axiosall';
import { GiftedChat, Send } from 'react-native-gifted-chat';
import moment from 'moment';
import colors from '../../constants/colors';
import api from '../../constants/api';
import images from '../../constants/images';
import { StyleSheet } from 'react-native';
import _ from 'lodash';

const Chat = props => {
    const [ messages, setMessages ] = useState([]);

    useEffect(() => {
        const recivedParams = props.route.params;
        //Changing HeaderTitle and headerLeft Logo on basis of each profile
        props.navigation.setOptions({
            //Title changing depending on props from ThreadsScreen or ConnectionsScreen
            title: recivedParams.name ? recivedParams.name : recivedParams.firstName + ' ' + recivedParams.lastName,
            headerLeft: () => (
                <Thumbnail circular
                    style={ { marginLeft: 15, width: 46, height: 46 } }
                    source={ {
                        uri: recivedParams.profilePic !== null
                            ? recivedParams.profilePic
                            : images.DEFAULT_PROFILE
                    } } />
            )
        });

        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: {
                accountId: accountDetails.accountId,
                //ConnectionId changing depending on props from ThreadsScreen or ConnectionsScreen
                connectionId: recivedParams.recipientId
                    ? recivedParams.recipientId
                    : recivedParams.connectionId !== accountDetails.accountId    //Switching Between connectionId and accountId from Connections API
                        ? recivedParams.connectionId
                        : recivedParams.accountId
            }
        };

        axiosall('get', api.GET_MESSAGES, configuration)
            ?.then((response) => {
                const messages = _.sortBy(response.data, "id");
                const result = messages.map((data, index) => {
                    return (
                        {
                            _id: data.id,
                            text: data.message,
                            createdAt: moment(data.createdAt, 'YYYY-MM-DD hh:mm:ss.SSS utc').add('05:30', 'hh:mm').format(),
                            user: {
                                _id: data.senderId,
                                name: recivedParams.name ? recivedParams.name : recivedParams.firstName + ' ' + recivedParams.lastName,
                                avatar: recivedParams.profilePic !== null ? recivedParams.profilePic : images.DEFAULT_PROFILE,
                            },
                        }
                    );
                })
                setMessages(result.reverse());
            })
            .catch((error) => {
                console.log(error);
            });
    })

    const onSend = useCallback((messages = []) => {
        const accountDetails = props.Auths[ 0 ].name;
        const addMessage = {
            senderId: messages[ 0 ].user._id,
            message: messages[ 0 ].text,
            recipientId: props.route.params.recipientId
                ? props.route.params.recipientId
                : props.route.params.connectionId !== accountDetails.accountId
                    ? props.route.params.connectionId
                    : props.route.params.accountId
        }

        let params = {
            url: api.BASE + api.ADD_MESSAGE,
            configuration: {
                headers: {
                    Authorization: "Bearer " + accountDetails.token
                },
            },
        }

        axios.post(params.url, addMessage, params.configuration)
            .then((response) => {
                if (response.status == '200') {
                    //Appending message to GiftedChat component
                    setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
                }
            })
            .catch((error) => {
                Toast.show({
                    text: error,
                    buttonText: 'Okay',
                    buttonTextStyle: { color: '#ccc' },
                    textStyle: { ...textStyles.bodyText, color: colors.base },
                    style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
                    duration: 3000
                })
            })
    }, [])

    return (
        <GiftedChat
            messages={ messages }
            onSend={ messages => onSend(messages) }
            user={ {
                _id: props.Auths[ 0 ].name.accountId,
            } }
            textStyle={ { color: colors.primary } }
            messagesContainerStyle={ { backgroundColor: colors.base } }
            renderSend={ (props) => (
                <Send
                    { ...props }
                    containerStyle={ styles.sendContainer }>
                    <Icon name='send-o' type='FontAwesome' style={ styles.sendIcon } />
                </Send>) }
        />
    )
};

const styles = StyleSheet.create({
    sendContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginHorizontal: 10,
    },

    sendIcon: {
        backgroundColor: colors.primary,
        color: colors.base,
        padding: 8,
        borderRadius: 10
    },
})

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Chat);