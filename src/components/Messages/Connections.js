import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { View, Text, List, ListItem, Body, Left, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../../util/axiosall';
import LoadingScreen from '../LoadingScreen';
import textStyles from '../../constants/textStyles';
import api from '../../constants/api';
import images from '../../constants/images';
import NoData from '../NoData';

const Connections = props => {
    const [ loading, setLoading ] = useState(false);
    const [ connections, setConnections ] = useState({
        confirmRequest: []
    });

    useEffect(() => {
        setLoading(true);
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: accountDetails.accountId }
        };

        axiosall('get', api.GET_CONNECTION, configuration)
            ?.then((response) => {
                //Only Accepting Confirmed Connections
                let confirmRequest = [];
                response.data.forEach(element => {
                    if (element.status === "accepted") {
                        confirmRequest.push(element)
                    }
                });
                setConnections({ confirmRequest })
                setLoading(false);
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }, []);

    if (loading) {
        return <LoadingScreen />;
    }

    const MessagesCards = itemData => {
        return (
            <List>
                <ListItem thumbnail onPress={ () => props.navigation.navigate('Chat', itemData.item) }>
                    <Left>
                        <Thumbnail source={ {
                            uri: itemData.item.profilePic != null
                                ? itemData.item.profilePic
                                : images.DEFAULT_PROFILE
                        } } />
                    </Left>
                    <Body>
                        <Text style={ { ...textStyles.regularText } } numberOfLines={ 2 }>
                            { itemData.item.firstName + " " + itemData.item.lastName }
                        </Text>
                        { itemData.item.latestJobTitle !== null
                            ? <Text style={ textStyles.bodyText }>{ itemData.item.latestJobTitle }</Text>
                            : null }
                    </Body>
                </ListItem>
            </List>
        );
    }

    return (
        <View style={ styles.screen }>
            { connections.confirmRequest[ 0 ] !== undefined
                ? <FlatList
                    keyExtractor={ (item, index) => item.id }
                    data={ connections.confirmRequest }
                    renderItem={ MessagesCards }
                />
                : <NoData message='Please Add Connections, to see them here...' style={ styles.noDataStyle } /> }
        </View>
    );
};


const styles = StyleSheet.create({
    screen: {
        flex: 1
    },

    noDataStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Connections);