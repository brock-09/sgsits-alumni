import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { View, Text } from 'native-base';
import images from '../constants/images';

const NoData = props => {
    return (
        <View style={ styles.container } { ...props }>
            <Image style={ styles.imageStyle } source={ images.NO_DATA } />
            <Text note>{ props.message }</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageStyle: {
        width: 150,
        height: 150,
    },

})

export default NoData;