import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text, List, ListItem, Thumbnail, Body, Left } from 'native-base';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import images from '../../constants/images';

const NotificationCard = props => {
    const result = props.data;
    return (
        <List style={ { backgroundColor: result.isRead ? colors.base : colors.unRead, paddingVertical: 5 } }>
            <ListItem thumbnail>
                <Left>
                    <Thumbnail source={ {
                        uri: result.profilePic != null
                            ? result.profilePic
                            : images.DEFAULT_PROFILE
                    } } />
                </Left>
                <Body>
                    { result.message
                        ? <Text style={ { ...textStyles.bodyText } } numberOfLines={ 3 }>{ result.message }</Text>
                        : null }
                    { result.status === 'Pending'
                        ? <View style={ styles.buttonContainer }>
                            <AppButton small onPress={ () => props.onResponse(result, "accepted") }>Accept</AppButton>
                            <AppButtonSec small onPress={ () => props.onResponse(result, 'rejected') }>Reject</AppButtonSec>
                        </View>
                        : null }
                </Body>
            </ListItem>
        </List>
    );
};

const styles = StyleSheet.create({

    buttonContainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
})

export default NotificationCard;
