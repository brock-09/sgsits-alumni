import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList, ScrollView } from 'react-native';
import { View, Text, Card, CardItem } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../../util/axiosall';
import LoadingScreen from '../LoadingScreen';
import ConnectionCardItem from '../ConnectionCardItem';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import api from '../../constants/api';

const ProfileConnection = props => {

    const [ loading, setLoading ] = useState(false);
    const [ connections, setConnections ] = useState({
        pendingRequests: [],
        confirmRequest: [],
        sentPendingRequests: [],
    });


    useEffect(() => {
        setLoading(true);
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;

        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: accountDetails.accountId }
        };


        axiosall('get', api.GET_CONNECTION, configuration)
            ?.then((response) => {
                //Dividing response Data into different arrays for divided displays
                let pendingRequests = [], confirmRequest = [], sentPendingRequests = [];
                response.data.forEach(element => {
                    if (element.status === "Pending") {
                        if (element.accountId !== accountDetails.accountId) {
                            pendingRequests.push(element)
                        } else {
                            sentPendingRequests.push(element)
                        }
                    } else if (element.status === "accepted") {
                        confirmRequest.push(element)
                    }
                });
                setConnections({ pendingRequests, confirmRequest, sentPendingRequests })
                setLoading(false);
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }, []);

    if (loading) {
        return <LoadingScreen />;
    }

    const pendingRequestsCards = result => {
        return (<ConnectionCardItem data={ result.item } { ...props } />)
    }
    const sentPendingRequestsCards = result => {
        return (<ConnectionCardItem data={ result.item } { ...props } />)
    }
    const confirmRequestCards = result => {
        return (<ConnectionCardItem data={ result.item } { ...props } />)
    }

    return (
        <ScrollView>
            <View style={ styles.container }>
                <Card>
                    <CardItem header bordered>
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Pending Connection Requests</Text>
                    </CardItem>
                    <View style={ styles.CardsContainer }>
                        { connections.pendingRequests[ 0 ] !== undefined
                            ? <FlatList
                                keyExtractor={ (item, index) => item.id }
                                data={ connections.pendingRequests }
                                renderItem={ pendingRequestsCards }
                                numColumns={ 2 }
                            />
                            : <Text style={ { ...textStyles.bodyText } }>No Pending Requests</Text> }
                    </View>
                </Card>

                <Card>
                    <CardItem header bordered>
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Sent Pending Connection Requests</Text>
                    </CardItem>
                    <View style={ styles.CardsContainer }>
                        { connections.sentPendingRequests[ 0 ] !== undefined
                            ? <FlatList
                                keyExtractor={ (item, index) => item.id }
                                data={ connections.sentPendingRequests }
                                renderItem={ sentPendingRequestsCards }
                                numColumns={ 2 }
                            />
                            : <Text style={ { ...textStyles.bodyText } }>No Pending Requests</Text> }
                    </View>
                </Card>

                <Card>
                    <CardItem header bordered>
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Connections</Text>
                    </CardItem>
                    <View style={ styles.CardsContainer }>
                        { connections.confirmRequest[ 0 ] !== undefined
                            ? <FlatList
                                keyExtractor={ (item, index) => item.id }
                                data={ connections.confirmRequest }
                                renderItem={ confirmRequestCards }
                                numColumns={ 2 }
                            />
                            : <Text style={ { ...textStyles.bodyText } }> No Connection Available</Text> }
                    </View>
                </Card>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        marginVertical: 10,
    },

    textStyle: {
        fontSize: 16,
        color: colors.primary
    },

    CardsContainer: {
        margin: 5
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(ProfileConnection);