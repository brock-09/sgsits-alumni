import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, ScrollView, Linking, FlatList } from 'react-native';
import { Card, CardItem, Body, Icon, Input, Form, Item, Label, DatePicker, Picker, Toast, CheckBox } from 'native-base';
import HTML from 'react-native-render-html';
import { TextInput } from 'react-native-paper';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { axiosall } from '../../util/axiosall';
import api from '../../constants/api';
import countryCodeList from '../../util/countryCodeList';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import LoadingScreen from '../LoadingScreen';
import colors from '../../constants/colors';
import textStyles from '../../constants/textStyles';
import ProfileEducationForm from './ProfileEducationForm';
import ProfileEducationFormPhD from './ProfileEducationFormPhD';

const ProfileDetail = props => {

    const [ UserDetailData, setUserDetailData ] = useState();
    const [ EducatioData, setEducationData ] = useState();
    const [ ProfessionalData, setProfessionalData ] = useState();
    const isMyProfile = true;
    const [ EditProfileDetailDisplay, setEditProfileDetailDisplay ] = useState(false);
    const [ EditAboutDisplay, setEditAboutDisplay ] = useState(false);
    const [ EditEducationDisplay, setEditEducationDisplay ] = useState(false);
    const [ EditExperienceDisplay, setEditExperienceDisplay ] = useState(false);

    useEffect(() => {
        //Fetching data through props from parent(Profile) Component
        if (props.data !== undefined) {
            //Dividing data into Sections
            setUserDetailData(props.data.profile);
            setEducationData(props.data.education);
            setProfessionalData(props.data.professional);
        }
    }, [ props ]);

    if (UserDetailData === undefined || EducatioData === undefined || ProfessionalData === undefined) {
        return <LoadingScreen />;
    }

    const EditProfileDetail = () => {

        const [ Country, setCountry ] = useState();
        const [ selectedCountry, setSelectedCountry ] = useState();
        const [ State, setState ] = useState();
        const [ selectedState, setSelectedState ] = useState();
        const [ Cities, setCities ] = useState();
        const [ countryCode, setCountryCode ] = useState(UserDetailData.phoneNumber.slice(0, UserDetailData.phoneNumber.length - 10));

        useEffect(() => {
            axiosall('get', api.GET_COUNTRIES)
                ?.then((response) => {
                    const result = response.data;
                    setCountry(result.rows);
                })
                .catch((error) => {
                    console.log('Profile Countries', error);
                });
        }, []);

        const initialValues = {
            firstName: UserDetailData.firstName,
            lastName: UserDetailData.lastName,
            phoneNumber: UserDetailData.phoneNumber.slice(UserDetailData.phoneNumber.length - 10, UserDetailData.phoneNumber.length),
            country: UserDetailData.country,
            state: UserDetailData.state,
            currentCity: UserDetailData.currentCity,
            instagram: UserDetailData.instagram,
            facebook: UserDetailData.facebook,
            twitter: UserDetailData.twitter,
            linkedIn: UserDetailData.linkedIn,
            website: UserDetailData.website
        }

        const phoneRegExp = /^(0|[1-9][0-9]{9})$/i
        const urlRegExp = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
        const facebookRegExp = /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)#!\/)?(?:pages\/)?(?:[\w\-]\/)([\w\-\.])/
        const twitterRegExp = /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/
        const instagramRegExp = /(?:(?:http|https):\/\/)?(?:www\.)?(?:instagram\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/
        const capitalize = /^[A-Z][a-z0-9_-]{2,19}$/

        const validationSchema = Yup.object({
            firstName: Yup.string()
                .strict(true)
                .trim('Whitespaces not allowed')
                .min(3, 'First Name Min. 3 Character')
                .matches(capitalize, 'First Name Not Valid')
                .required('Required'),
            lastName: Yup.string()
                .strict(true)
                .trim('Whitespaces not allowed')
                .min(3, 'Last Name Min. 3 Character')
                .matches(capitalize, 'Last Name Not Valid')
                .required('Required'),
            phoneNumber: Yup.string()
                .matches(phoneRegExp, 'Phone Number Not Valid')
                .min(10, 'Min. characters 10')
                .required('Required'),
            instagram: Yup.string().matches(instagramRegExp, 'Link Not Valid'),
            facebook: Yup.string().matches(facebookRegExp, 'Link Not Valid'),
            twitter: Yup.string().matches(twitterRegExp, 'Link Not Valid'),
            linkedIn: Yup.string().matches(urlRegExp, 'Link Not Valid'),
            website: Yup.string().matches(urlRegExp, 'Link Not Valid'),
            country: Yup.string().required('Required'),
            state: Yup.string().required('Required'),
            currentCity: Yup.string().required('Required')
        })

        const onSubmit = values => {
            //Fetching userAccountDetails for token and Account from AuthReducer
            const accountDetails = props.Auths[ 0 ].name;
            let mobile = countryCode + formik.values.phoneNumber;
            let params = {
                url: api.BASE + api.UPDATE_PROFILE,
                configuration: {
                    headers: {
                        'Authorization': "Bearer " + accountDetails.token
                    }
                },
                data: {
                    accountId: accountDetails.accountId,
                    personal: {
                        ...formik.values,
                        phoneNumber: mobile
                    }
                }
            }

            axios.put(params.url, params.data, params.configuration)
                .then((response) => {
                    //Updating ProfileDetail with the updatedData
                    const result = response.data.data[ 0 ]
                    setUserDetailData(result)
                    Toast.show({
                        text: 'Profile Details Updated',
                        buttonText: 'Okay',
                        buttonTextStyle: { color: '#ccc' },
                        textStyle: { ...textStyles.bodyText, color: colors.base },
                        style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
                        duration: 3000
                    })
                })
                .catch((error) => {
                    console.log('EditProfile Update', error)
                });

            return setEditProfileDetailDisplay(false);
        }

        const onReset = values => {
            formik.initialValues
            return setEditProfileDetailDisplay(false);
        }

        const formik = useFormik({
            initialValues,
            onSubmit,
            onReset,
            validationSchema
        })

        const handleCountry = value => {
            setSelectedCountry(value)
            formik.setFieldValue('country', value.name)
            axiosall('get', `/getStatesByCountry?id=${ value.id }`)
                ?.then((response) => {
                    const result = response.data;
                    setState(result.rows)
                })
                .catch((error) => {
                    console.log('Profile States', error);
                });
        }

        const handleState = value => {
            setSelectedState(value)
            formik.setFieldValue('state', value.name)
            axiosall('get', `/getCitiesByState?id=${ value.id }`)
                ?.then((response) => {
                    const result = response.data;
                    setCities(result.rows);
                })
                .catch((error) => {
                    console.log('Profile States', error);
                });
        }

        return (
            <View style={ styles.EditProfileDetailContainer }>
                <Form>
                    <TextInput
                        label='First Name'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('firstName') }
                        onChangeText={ formik.handleChange('firstName') }
                        value={ formik.values.firstName }
                        error={ formik.touched.firstName && formik.errors.firstName ? true : false } />
                    { formik.touched.firstName && formik.errors.firstName ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.firstName }</Text> : null }

                    <TextInput
                        label='Last Name'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('lastName') }
                        onChangeText={ formik.handleChange('lastName') }
                        value={ formik.values.lastName }
                        error={ formik.touched.lastName && formik.errors.lastName ? true : false } />
                    { formik.touched.lastName && formik.errors.lastName ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.lastName }</Text> : null }

                    <Item last>
                        <Picker
                            mode="dropdown"
                            placeholder="drop"
                            style={ { width: '40%' } }
                            selectedValue={ countryCode }
                            onValueChange={ (value) => value !== '' ? setCountryCode(value) : null }>
                            <Picker.Item label="Code" value='' />
                            { countryCodeList !== undefined
                                ? countryCodeList.map((data, index) => {
                                    return <Picker.Item key={ index } label={ data.label + ' ' + data.value } value={ data.value } />
                                })
                                : <Picker.Item label='Loading' value='' /> }
                        </Picker>
                        <TextInput
                            style={ { width: '60%', marginBottom: -2 } }
                            label='Mobile No'
                            mode='flat'
                            underlineColor='transparent'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                            dense={ true }
                            keyboardType="numeric"
                            returnKeyType='next'
                            maxLength={ 10 }
                            onBlur={ formik.handleBlur('phoneNumber') }
                            onChangeText={ formik.handleChange('phoneNumber') }
                            value={ formik.values.phoneNumber }
                            error={ formik.touched.phoneNumber && formik.errors.phoneNumber ? true : false } />
                    </Item>
                    { formik.touched.phoneNumber && formik.errors.phoneNumber ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.phoneNumber }</Text> : null }

                    <Item last>
                        <Picker
                            mode="dropdown"
                            placeholder="Country"
                            selectedValue={ selectedCountry }
                            onValueChange={ (value) => value !== '' ? handleCountry(value) : null }>

                            <Picker.Item label={ formik.values.country } value='' />
                            { Country !== undefined
                                ? Country.map((data, index) => {
                                    return <Picker.Item key={ index } label={ data.name } value={ data } />
                                })
                                : <Picker.Item label='Loading' value='' /> }
                        </Picker>
                    </Item>


                    <Item last>
                        <Picker
                            mode="dropdown"
                            placeholder="State"
                            selectedValue={ selectedState }
                            onValueChange={ (value) => value !== '' ? handleState(value) : null }>

                            <Picker.Item label={ formik.values.state } value='' />
                            { State !== undefined
                                ? State.map((data, index) => {
                                    return <Picker.Item key={ index } label={ data.name } value={ data } />
                                })
                                : <Picker.Item label='Please Select Country' value='' /> }
                        </Picker>
                    </Item>

                    <Item last>
                        <Picker
                            mode="dropdown"
                            placeholder="City"
                            selectedValue={ formik.values.currentCity }
                            onValueChange={ (value) => value !== '' ? formik.setFieldValue('currentCity', value) : null }>

                            <Picker.Item label={ formik.values.currentCity } value='' />
                            { Cities !== undefined
                                ? Cities.map((data, index) => {
                                    return <Picker.Item key={ index } label={ data.name } value={ data.name } />
                                })
                                : <Picker.Item label='City' value='' /> }
                        </Picker>
                    </Item>

                    <TextInput
                        label='Instagram Link'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        keyboardType='url'
                        textContentType='URL'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('instagram') }
                        onChangeText={ formik.handleChange('instagram') }
                        value={ formik.values.instagram }
                        error={ formik.touched.instagram && formik.errors.instagram ? true : false } />
                    { formik.touched.instagram && formik.errors.instagram ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.instagram }</Text> : null }

                    <TextInput
                        label='Twitter Link'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        keyboardType='url'
                        textContentType='URL'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('twitter') }
                        onChangeText={ formik.handleChange('twitter') }
                        value={ formik.values.twitter }
                        error={ formik.touched.twitter && formik.errors.twitter ? true : false } />
                    { formik.touched.twitter && formik.errors.twitter ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.twitter }</Text> : null }

                    <TextInput
                        label='Facebook Link'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        keyboardType='url'
                        textContentType='URL'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('facebook') }
                        onChangeText={ formik.handleChange('facebook') }
                        value={ formik.values.facebook }
                        error={ formik.touched.facebook && formik.errors.facebook ? true : false } />
                    { formik.touched.facebook && formik.errors.facebook ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.facebook }</Text> : null }

                    <TextInput
                        label='LinkedIn Link'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        keyboardType='url'
                        textContentType='URL'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('linkedIn') }
                        onChangeText={ formik.handleChange('linkedIn') }
                        value={ formik.values.linkedIn }
                        error={ formik.touched.linkedIn && formik.errors.linkedIn ? true : false } />
                    { formik.touched.linkedIn && formik.errors.linkedIn ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.linkedIn }</Text> : null }

                    <TextInput
                        label='Personal Website Link'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        keyboardType='url'
                        textContentType='URL'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('website') }
                        onChangeText={ formik.handleChange('website') }
                        value={ formik.values.website }
                        error={ formik.touched.website && formik.errors.website ? true : false } />
                    { formik.touched.website && formik.errors.website ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.website }</Text> : null }

                    <View style={ styles.buttonContainer }>
                        <AppButton title='Done' type='submit' onPress={ formik.handleSubmit }>Done</AppButton>
                        <AppButtonSec title='Cancel' type='reset' onPress={ formik.handleReset }>Cancel</AppButtonSec>
                    </View>
                </Form>
            </View>
        )
    }

    const EditEducationDetail = () => {

        const [ loading, setLoading ] = useState(false);
        const [ PGCourses, setPGCourses ] = useState();
        const [ UGCourses, setUGCourses ] = useState();
        const [ PHDCourses, setPHDCourses ] = useState();
        const [ type, setType ] = useState('');

        const [ ugPresent, setugPresent ] = useState(false);
        const [ pgPresent, setpgPresent ] = useState(false);
        const [ phdPresent, setphdPresent ] = useState(false);

        useEffect(() => {
            setLoading(true)
            axiosall('get', api.GET_ALL_COURSES)
                ?.then((response) => {
                    const result = response.data;
                    setUGCourses(result.UG);
                    setPGCourses(result.PG);
                    setPHDCourses(result.PHD);
                    setLoading(false)
                })
                .catch((error) => {
                    console.log(error);
                });
        }, [ props ])

        const EducationCard = itemData => {
            if (itemData.item.type === 'ug' && itemData.item.collegeName) {
                setugPresent(true);
            }
            else if (itemData.item.type === 'pg' && itemData.item.collegeName) {
                setpgPresent(true);
            }
            else if (itemData.item.type === 'phd' && itemData.item.collegeName) {
                setphdPresent(true);
            }

            const onDelete = data => {
                const accountDetails = props.Auths[ 0 ].name;
                let params = {
                    url: api.BASE + api.DELETE_PROFILE_DATA + `?educationId=${ data.educationId }`,
                    configuration: {
                        headers: {
                            'Authorization': "Bearer " + accountDetails.token
                        }
                    }
                }

                axios.delete(params.url, params.configuration)
                    .then((response) => {
                        setEducationData(
                            EducatioData.filter(item => {
                                if (item.educationId !== data.educationId) {
                                    return data
                                }
                            })
                        );

                        Toast.show({
                            text: response.data.msg,
                            buttonText: 'Okay',
                            buttonTextStyle: { color: '#ccc' },
                            textStyle: { ...textStyles.bodyText, color: colors.base },
                            style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
                            duration: 3000
                        });
                    })
                    .catch((error) => {
                        console.log(error)
                    });
            }
            return (
                <CardItem style={ { backgroundColor: itemData.item.collegeName ? colors.unRead : null } }>
                    <Body>
                        { itemData.item.collegeName ? <Text style={ { ...textStyles.bodyText, fontSize: 8 } }>
                            From Shri Govindram Seksaria Institute of Technology and Science, Indore
                                </Text> : null }
                        <Text style={ { ...textStyles.regularText, fontSize: 14 } }>
                            { itemData.item.degree + ' - ' + itemData.item.courseName }{ '\n' }{ itemData.item.isPartTme ? '(Part Time)' : '(Full Time)' }
                        </Text>
                        <Text style={ textStyles.bodyText }>
                            { itemData.item.yearOfPassing }
                        </Text>
                    </Body>
                    { !itemData.item.collegeName ?
                        <Icon name='delete' type='MaterialCommunityIcons' style={ styles.Icons } onPress={ () => onDelete(itemData.item) } />
                        : null }
                </CardItem>
            );
        }

        if (loading && PGCourses === undefined && UGCourses === undefined && PHDCourses === undefined) {
            return <LoadingScreen />;
        }

        let formOutput;

        if (type == 'UG') {
            formOutput = (
                <View style={ styles.formContainer }>
                    <Text style={ textStyles.regularText }>Add UG Course</Text>
                    <ProfileEducationForm type={ type } data={ UGCourses } EducationDisplay={ () => setEditEducationDisplay(true) } />
                </View>
            )
        }
        else if (type === 'PG') {
            formOutput = (
                <View style={ styles.formContainer }>
                    <Text style={ textStyles.regularText }>Add PG Course</Text>
                    <ProfileEducationForm type={ type } data={ PGCourses } EducationDisplay={ () => setEditEducationDisplay(true) } />
                </View>
            )
        }
        else if (type == 'PHD') {
            formOutput = (
                <View style={ styles.formContainer }>
                    <Text style={ textStyles.regularText }>Add PhD Course</Text>
                    <ProfileEducationFormPhD type={ type } data={ PHDCourses } EducationDisplay={ () => setEditEducationDisplay(true) } />
                </View>
            )
        }
        return (
            <View>
                { EditEducationDisplay ? <View style={ styles.screen }>
                    <CheckBox
                        onPress={ () => setType('UG') }
                        disabled={ ugPresent }
                        checked={ type === 'UG' || ugPresent } color={ colors.primary } />
                    <Text>UG</Text>
                    <CheckBox
                        onPress={ () => setType('PG') }
                        disabled={ pgPresent }
                        checked={ type === 'PG' || pgPresent } color={ colors.primary } />
                    <Text>PG</Text>
                    <CheckBox
                        onPress={ () => setType('PHD') }
                        disabled={ phdPresent }
                        checked={ type === 'PHD' || phdPresent } color={ colors.primary } />
                    <Text>PhD</Text>
                </View>
                    : null }
                { EditEducationDisplay ?
                    <View style={ styles.EditProfileDetailContainer }>
                        { formOutput }
                    </View>
                    : null }

                <View>
                    { EducatioData !== undefined
                        ? <FlatList
                            keyExtractor={ (item, index) => index }
                            data={ EducatioData }
                            renderItem={ EducationCard } />
                        : <Text style={ { ...textStyles.regularText, ...styles.Text } }>No Data Found</Text> }
                </View>
            </View>
        );
    }

    const EditExperienceDetail = () => {

        const [ startDate, setStartDate ] = useState(new Date());

        const initialValues = {
            id: Math.random(),
            title: '',
            companyName: '',
            location: '',
            type: '',
            monthOfJoining: '',
            yearOfJoining: '',
            monthOfLeaving: '',
            yearOfLeaving: '',
            isWorking: false
        };

        const onSubmit = values => {
            //Fetching userAccountDetails for token and Account from AuthReducer
            const accountDetails = props.Auths[ 0 ].name;

            let params = {
                url: api.BASE + api.UPDATE_PROFILE,
                configuration: {
                    headers: {
                        'Authorization': "Bearer " + accountDetails.token
                    }
                },
                data: {
                    accountId: accountDetails.accountId,
                    professional: values
                }
            }

            axios.put(params.url, params.data, params.configuration)
                .then((response) => {
                    //Updating ProfileDetail with the updatedData
                    const result = response.data.data;
                    //  setUserDetailData(result)
                    setProfessionalData([ ...ProfessionalData, result ]);

                    Toast.show({
                        text: 'Profile Experience Updated',
                        buttonText: 'Okay',
                        buttonTextStyle: { color: '#ccc' },
                        textStyle: { ...textStyles.bodyText, color: colors.base },
                        style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
                        duration: 3000
                    })
                })
                .catch((error) => {
                    console.log(error)
                });

            return setEditExperienceDisplay(false);
        }

        const onReset = values => {
            formik.initialValues
            setEditExperienceDisplay(false);
        }

        const validationSchema = Yup.object({
            title: Yup.string()
                .strict(true)
                .trim('Whitespaces not allowed')
                .required('Required'),
            companyName: Yup.string()
                .strict(true)
                .trim('Whitespaces not allowed')
                .required('Required'),
            location: Yup.string()
                .strict(true)
                .trim('Whitespaces not allowed')
                .required('Required'),
            type: Yup.string().required('Required'),
            monthOfJoining: Yup.string().required('Required'),
            monthOfLeaving: Yup.string()
                .when('isWorking', {
                    is: false,
                    otherwise: Yup.string().notRequired(),
                    then: Yup.string().required('Required')
                }),

        });

        const formik = useFormik({
            initialValues,
            onSubmit,
            onReset,
            validationSchema
        });

        const handleStartDate = Date => {
            setStartDate(Date);
            const monthNames = [ "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December" ];
            formik.setFieldValue('monthOfJoining', monthNames[ Date.getMonth() ]);
            formik.setFieldValue('yearOfJoining', Date.getFullYear());
        }

        const handleEndDate = Date => {
            const monthNames = [ "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December" ];
            formik.setFieldValue('monthOfLeaving', monthNames[ Date.getMonth() ]);
            formik.setFieldValue('yearOfLeaving', Date.getFullYear());
        }

        return (
            <View style={ styles.EditProfileDetailContainer }>
                <Form>
                    <TextInput
                        label='Job Title'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('title') }
                        onChangeText={ formik.handleChange('title') }
                        value={ formik.values.title } />
                    { formik.touched.title && formik.errors.title
                        ? <Text style={ styles.errors }>{ formik.errors.title }</Text>
                        : null }

                    <TextInput
                        label='Company Name'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('companyName') }
                        onChangeText={ formik.handleChange('companyName') }
                        value={ formik.values.companyName } />
                    { formik.touched.companyName && formik.errors.companyName
                        ? <Text style={ styles.errors }>{ formik.errors.companyName }</Text>
                        : null }

                    <TextInput
                        label='Location'
                        mode='flat'
                        theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                        dense={ true }
                        autoCapitalize='words'
                        returnKeyType='next'
                        onBlur={ formik.handleBlur('location') }
                        onChangeText={ formik.handleChange('location') }
                        value={ formik.values.location } />
                    { formik.touched.location && formik.errors.location
                        ? <Text style={ styles.errors }>{ formik.errors.location }</Text>
                        : null }

                    <Item last>
                        <Label style={ textStyles.bodyText }>Type</Label>
                        <Picker
                            mode="dropdown"
                            placeholder="Type"
                            selectedValue={ formik.values.type }
                            onValueChange={ (value) => value !== '' ? formik.setFieldValue('type', value) : null }>
                            <Picker.Item label="Type" value='' />
                            <Picker.Item label="Part-Time" value="Part-Time" />
                            <Picker.Item label="Full-Time" value="Full-Time" />
                            <Picker.Item label="Contract" value="Contract" />
                        </Picker>
                    </Item>

                    <Item last>
                        <Label style={ { ...textStyles.bodyText, ...styles.Date } }>From</Label>
                        <DatePicker
                            minimumDate={ new Date(1950, 0) }
                            maximumDate={ new Date() }
                            locale={ "en" }
                            modalTransparent={ true }
                            animationType={ "fade" }
                            androidMode={ "default" }
                            placeHolderText="Date"
                            placeHolderTextStyle={ { color: "#d3d3d3" } }
                            onDateChange={ date => { handleStartDate(date) } }
                            disabled={ false } />
                    </Item>

                    <Item last>
                        <Label style={ { ...textStyles.bodyText, ...styles.Date } }>To</Label>
                        { formik.values.isWorking != true
                            ? <DatePicker
                                minimumDate={ new Date(startDate) }
                                maximumDate={ new Date() }
                                locale={ "en" }
                                modalTransparent={ true }
                                animationType={ "fade" }
                                androidMode={ "default" }
                                placeHolderText="Date"
                                placeHolderTextStyle={ { color: "#d3d3d3" } }
                                onDateChange={ date => { handleEndDate(date) } }
                                disabled={ false } />
                            : null }

                        <CheckBox
                            onPress={ () => formik.setFieldValue('isWorking', (formik.values.isWorking ? false : true)) }
                            checked={ formik.values.isWorking === true }
                            color={ colors.primary } />
                        <Text
                            style={ styles.checkboxText }
                            onPress={ () => formik.setFieldValue('isWorking', (formik.values.isWorking ? false : true)) }>
                            Currently working here
                                </Text>
                    </Item>
                    { formik.errors.monthOfJoining || formik.errors.monthOfLeaving ? <Text style={ styles.errors }>{ 'Fill all Details' }</Text> : null }
                </Form>

                <View style={ styles.buttonContainer }>
                    <AppButton onPress={ formik.handleSubmit }>Add</AppButton>
                    <AppButtonSec type='reset' onPress={ formik.handleReset }>Cancel</AppButtonSec>
                </View>
            </View>
        );
    }

    const onExperienceDelete = (data) => {

        const accountDetails = props.Auths[ 0 ].name;
        let params = {
            url: api.BASE + api.DELETE_PROFILE_DATA + `?professionalId=${ data.id }`,
            configuration: {
                headers: {
                    'Authorization': "Bearer " + accountDetails.token
                }
            }
        }

        axios.delete(params.url, params.configuration)
            .then((response) => {
                setProfessionalData(
                    ProfessionalData.filter(item => {
                        if (item.id !== data.id) {
                            return data
                        }
                    })
                );

                Toast.show({
                    text: response.data.msg,
                    buttonText: 'Okay',
                    buttonTextStyle: { color: '#ccc' },
                    textStyle: { ...textStyles.bodyText, color: colors.base },
                    style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
                    duration: 3000
                });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    return (
        <ScrollView>
            { !EditProfileDetailDisplay ? <View style={ styles.PersonalDetailContainer }>
                { UserDetailData.firstName || UserDetailData.lastName || isMyProfile
                    ? <View style={ { marginBottom: 10 } }>
                        <Text style={ { ...textStyles.regularText, ...styles.Name } }>
                            <Text style={ { textAlign: 'center' } }>
                                { UserDetailData.firstName + " " + UserDetailData.lastName }
                            </Text>
                            <Text> </Text>
                            <Icon name='pencil' type='FontAwesome' style={ { ...styles.EditIcon, fontSize: 16 } } onPress={ () => { setEditProfileDetailDisplay(true) } } />
                        </Text>
                    </View>
                    : null }

                { UserDetailData.email ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Email</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.email }</Text>
                </View> : null }
                { UserDetailData.gender ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Gender</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.gender }</Text>
                </View> : null }
                { UserDetailData.dateOfBirth ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Date of Birth</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ new Date(UserDetailData.dateOfBirth).toDateString() }</Text>
                </View> : null }
                { UserDetailData.phoneNumber ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Mobile</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.phoneNumber }</Text>
                </View> : null }
                { UserDetailData.country ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Country</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.country }</Text>
                </View> : null }
                { UserDetailData.state ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>State</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.state }</Text>
                </View> : null }
                { UserDetailData.currentCity ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>City</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.currentCity }</Text>
                </View> : null }
                { UserDetailData.userCode ? < View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Alumni Unique Id</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.userCode }</Text>
                </View> : null }

                <View style={ styles.SocialLinksContainer }>
                    { UserDetailData.instagram ? <View><Icon name='instagram' type='FontAwesome' style={ { color: colors.instagram } } onPress={ () => Linking.openURL(UserDetailData.instagram) } /></View> : null }
                    { UserDetailData.facebook ? <View><Icon name='facebook-square' type='FontAwesome' style={ { color: colors.facebook } } onPress={ () => Linking.openURL(UserDetailData.facebook) } /></View> : null }
                    { UserDetailData.twitter ? <View><Icon name='twitter-square' type='FontAwesome' style={ { color: colors.twitter } } onPress={ () => Linking.openURL(UserDetailData.twitter) } /></View> : null }
                    { UserDetailData.linkedIn ? <View><Icon name='linkedin-square' type='FontAwesome' style={ { color: colors.linkdein } } onPress={ () => Linking.openURL(UserDetailData.linkedIn) } /></View> : null }
                    { UserDetailData.website ? <View><Icon name='web' type='MaterialCommunityIcons' onPress={ () => Linking.openURL(UserDetailData.website) } /></View> : null }
                </View>
            </View> : <EditProfileDetail /> }


            <View style={ styles.CardContainer }>
                <Card>
                    <CardItem header bordered>
                        <Icon name='file-text' type='Feather' style={ { color: colors.primary } } />
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>
                            About
                        </Text>
                        <Icon name='pencil' type='FontAwesome' style={ styles.EditIcon } />
                        {/* onPress={ () => { setEditAboutDisplay(true) } } */ }
                    </CardItem>
                    <CardItem>
                        <Body>
                            { !EditAboutDisplay
                                ? <HTML html={ UserDetailData.aboutMe ? UserDetailData.aboutMe : '<p>Write something about yourself...</p>' } tagsStyles={ { p: textStyles.bodyText } } />
                                : null }
                        </Body>
                    </CardItem>
                </Card>

                <Card>
                    <CardItem header bordered>
                        <Icon name='university' type='FontAwesome' style={ { color: colors.primary } } />
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Education</Text>
                        <Icon name='ios-add' type='Ionicons' style={ { ...styles.EditIcon, fontSize: 22 } } onPress={ () => setEditEducationDisplay(true) } />
                    </CardItem>
                    <EditEducationDetail />
                </Card>

                <Card>
                    <CardItem header bordered>
                        <Icon name='building' type='FontAwesome' style={ { color: colors.primary } } />
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Experience</Text>
                        <Icon name='ios-add' type='Ionicons' style={ { ...styles.EditIcon, fontSize: 22 } } onPress={ () => setEditExperienceDisplay(true) } />
                    </CardItem>
                    {
                        EditExperienceDisplay
                            ? <EditExperienceDetail />
                            : null
                    }
                    { ProfessionalData[ 0 ] !== undefined ? ProfessionalData.map(data =>
                        <CardItem key={ data.id }>
                            <Body>
                                <Text style={ textStyles.regularText }>
                                    { data.companyName }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.title + ' ' + '(' + data.type + ')' }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.location }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.monthOfJoining + '-' + data.yearOfJoining + ' ' + 'to' + ' ' }{ data.isWorking ? 'Present' : data.monthOfLeaving + '-' + data.yearOfLeaving }
                                </Text>
                            </Body>
                            <Icon name='delete' type='MaterialCommunityIcons' style={ styles.Icons } onPress={ () => onExperienceDelete(data) } />
                        </CardItem>
                    ) : !EditExperienceDisplay
                            ? <CardItem><Body><Text style={ textStyles.bodyText }>No Data</Text></Body></CardItem>
                            : null }
                </Card>
            </View>
        </ScrollView >
    );
};

const styles = StyleSheet.create({
    PersonalDetailContainer: {
        marginHorizontal: 50,
        marginVertical: 5,
    },

    screen: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },

    EditProfileDetailContainer: {
        flex: 1,
        marginHorizontal: 20,
        marginVertical: 10,
    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    },

    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },

    EditIcon: {
        flex: 1,
        color: colors.primary,
        textAlign: 'right',
        fontSize: 18
    },

    Icons: {
        color: colors.primary,
        marginHorizontal: 5,
    },

    DetailContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: 5
    },

    textStyle: {
        color: colors.primary
    },

    DName: {
        fontSize: 14,
        color: '#ccc',
    },

    Name: {
        fontSize: 18,
        color: colors.primary,
        textAlign: 'center',
        marginTop: 10,
    },

    DText: {
        fontSize: 12,
    },

    Dtext: {
        fontSize: 14,
        color: colors.primary,
        textAlign: 'right',
    },

    CardContainer: {
        padding: 10,
    },

    SocialLinksContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: 10
    },

    checkboxText: {
        marginHorizontal: 20,
    },

    Date: {
        marginVertical: 16,
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(ProfileDetail);