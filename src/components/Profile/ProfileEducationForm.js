import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Form, Picker, Item, Text } from 'native-base';
import { TextInput } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import _ from 'lodash';
import { connect } from 'react-redux';
import axios from 'axios';
import { axiosall } from '../../util/axiosall';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import LoadingScreen from '../LoadingScreen';
import colors from '../../constants/colors';
import api from '../../constants/api';


const ProfileEducationForm = props => {

    const EducationData = props.data;

    const [ degrees, setdegrees ] = useState();
    const [ courses, setCourses ] = useState();
    const [ selectedCourse, setSelectedCourse ] = useState();
    const [ dateShow, setDateShow ] = useState(false);

    const navigation = useNavigation();

    const initialValues = {
        type: props.type.toLowerCase(),
        degree: '',
        courseId: '',
        courseName: '',
        rollNumber: '',
        yearOfPassing: '',
        isPartTime: '',
    }

    const validationSchema = Yup.object({
        rollNumber: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed'),
        degree: Yup.string().required('Required'),
        courseName: Yup.string().required('Required'),
        yearOfPassing: Yup.string().required('Required'),
        isPartTime: Yup.bool().required('Required'),
    })

    const handleAdd = () => {
        const accountDetails = props.Auths[ 0 ].name;
        //Adding type again to override initialValues type
        const data = {
            ...formik.values,
            type: props.type.toLowerCase()
        }
        let params = {
            url: api.BASE + api.UPDATE_PROFILE,
            configuration: {
                headers: {
                    'Authorization': "Bearer " + accountDetails.token
                }
            },
            data: {
                accountId: accountDetails.accountId,
                educational: [ data ]
            }
        }

        axios.put(params.url, params.data, params.configuration)
            .then((response) => {
                if (response) {
                    return navigation.replace('Profile');
                }
            })
            .catch((error) => {
                console.log('ProfileEducation', error);
                return navigation.replace('Profile');
            })
    }

    const onSubmit = values => {

        const data = {
            courseName: values.courseName,
            degree: values.degree,
            isPartTime: values.isPartTime
        }

        axiosall('get', api.VALIDATE_COURSE, { params: data })
            ?.then((response) => {
                if (response.data === true) {
                    handleAdd()
                }
            })
            .catch((error) => {
                console.log('Error', error);
                Alert.alert(
                    '',
                    'Course Type is Invalid for Course: ' + values.courseName,
                    [ { text: 'ok' } ]
                )
            })
    }

    const onReset = () => {
        formik.handleReset
        return navigation.replace('Profile');
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        onReset,
        validationSchema
    })

    useEffect(() => {
        //To Resetform on switching between UG and PG
        setSelectedCourse();

        if (EducationData) {
            //Fetching Degree from Education Data
            let degrees = [];
            EducationData.map(data => {
                return degrees = [ ...degrees, { label: data.degree.trim(), value: data.degree.trim() } ];
            });
            degrees = _.uniqBy(degrees, "label");
            setdegrees(degrees)
        } else {
            return null;
        }
    }, [ EducationData, props ])

    const handleDegree = value => {
        formik.setFieldValue('degree', value);
        //Initializing Course Array by filtering EducationData
        const courseData = EducationData.filter(data => {
            return data.degree.includes(value)
        });
        //Fetching Courses dependent on the Degree
        if (courseData) {
            let courses = [];
            courseData.map(data => {
                return courses = [ ...courses, { label: data.courseName, value: data.id } ];
            });
            //Getting Unique Courses to avoid duplicacy
            courses = _.uniqBy(courses, "label");
            setCourses(courses)
        }
    }

    const handleCourse = (value) => {
        setSelectedCourse(value)
        formik.setFieldValue('courseName', value.label)
        formik.setFieldValue('courseId', value.value)
    }

    const handlePassingYear = Date => {
        setDateShow(false);
        {
            Date !== undefined
                ? formik.setFieldValue('yearOfPassing', Date.getFullYear())
                : null
        }
    }

    if (EducationData === undefined) {
        return <LoadingScreen />;
    }

    return (
        <Form>
            <TextInput
                label='Enrollment No'
                placeholder='(Optional)'
                mode='flat'
                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                dense={ true }
                autoCapitalize='words'
                returnKeyType='next'
                onBlur={ formik.handleBlur('rollNumber') }
                onChangeText={ formik.handleChange('rollNumber') }
                value={ formik.values.rollNumber } />

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Degree"
                    selectedValue={ formik.values.degree }
                    onValueChange={ (value) => value !== '' ? handleDegree(value) : null }>
                    <Picker.Item label="Degree" value='' />
                    { degrees !== undefined
                        ? degrees.map((data, index) => {
                            return <Picker.Item key={ index } label={ data.label } value={ data.value } />
                        })
                        : <Picker.Item label='Loading' value='' /> }
                </Picker>
            </Item>

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Course"
                    selectedValue={ selectedCourse }
                    onValueChange={ (value) => value !== '' ? handleCourse(value) : null } >
                    <Picker.Item label="Course" value='' />
                    { courses !== undefined
                        ? courses.map((data, index) => {
                            return <Picker.Item key={ index } label={ data.label } value={ data } />
                        })
                        : <Picker.Item label='Loading' value='' /> }
                </Picker>
            </Item>

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Course Type"
                    selectedValue={ formik.values.isPartTime }
                    onValueChange={ (value) => value !== '' ? formik.setFieldValue('isPartTime', value) : null }>
                    <Picker.Item label="Type" value='' />
                    <Picker.Item label="Full-Time" value={ false } />
                    <Picker.Item label="Part-Time" value={ true } />
                </Picker>
            </Item>

            <Item last>
                <Text style={ { ...textStyles.regularText, ...styles.DatePickerText } } onPress={ () => setDateShow(true) } >
                    { formik.values.yearOfPassing !== '' ? formik.values.yearOfPassing : 'Year' }
                </Text>
                { dateShow
                    ? <DateTimePicker
                        minimumDate={ new Date(1950, 0) }
                        maximumDate={ new Date() }
                        value={ new Date(new Date().getFullYear(), new Date().getMonth()) }
                        mode='date'
                        display='default'
                        onChange={ (event, date) => handlePassingYear(date) }
                    />
                    : null }
            </Item>
            { formik.errors.degree || formik.errors.courseName || formik.errors.yearOfPassing || formik.errors.isPartTime ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>Please Select all Course Details</Text> : null }

            <View style={ styles.buttonContainer }>
                <AppButton onPress={ formik.handleSubmit }>Add</AppButton>
                <AppButtonSec type='reset' onPress={ () => onReset() }>Cancel</AppButtonSec>
            </View>
        </Form>

    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },

    DatePickerText: {
        marginHorizontal: 10,
        marginVertical: 15,
        fontSize: 16

    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(ProfileEducationForm);