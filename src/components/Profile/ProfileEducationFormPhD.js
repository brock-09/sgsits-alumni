import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { Form, Label, Picker, Item, Text } from 'native-base';
import { TextInput } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import _ from 'lodash';
import { connect } from 'react-redux';
import { addEducation } from '../../redux/actions/RegEducationalActions';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import LoadingScreen from '../LoadingScreen';
import colors from '../../constants/colors';
import api from '../../constants/api';
import axios from 'axios';

const ProfileEducationFormPhD = props => {

    const EducationData = props.data;

    const [ courses, setCourses ] = useState();
    const [ selectedCourse, setSelectedCourse ] = useState();
    const [ dateShow, setDateShow ] = useState(false);

    const navigation = useNavigation();

    const initialValues = {
        type: props.type.toLowerCase(),
        degree: 'Ph.D.',
        courseId: '',
        courseName: '',
        rollNumber: '',
        yearOfPassing: '',
        isPartTime: false,
    }

    const validationSchema = Yup.object({
        rollNumber: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed'),
        courseName: Yup.string().required('Required'),
        yearOfPassing: Yup.string().required('Required')
    })

    const onSubmit = values => {
        const accountDetails = props.Auths[ 0 ].name;
        //Adding type again to override initialValues type
        const data = {
            ...formik.values,
            type: props.type.toLowerCase()
        }

        let params = {
            url: api.BASE + api.UPDATE_PROFILE,
            configuration: {
                headers: {
                    'Authorization': "Bearer " + accountDetails.token
                }
            },
            data: {
                accountId: accountDetails.accountId,
                education: data
            }
        }
        axios.put(params.url, params.data, params.configuration)
            .then((response) => {
                if (response) {
                    return navigation.replace('Profile');
                }
            })
            .catch((error) => {
                console.log('ProfileEducation', error);
                return navigation.replace('Profile');
            })
    }

    const onReset = () => {
        formik.handleReset
        return navigation.replace('Profile');
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        onReset,
        validationSchema
    })

    if (EducationData === undefined) {
        return <LoadingScreen />;
    }

    useEffect(() => {
        if (EducationData) {
            let courses = [];
            EducationData.map(data => {
                return courses = [ ...courses, { label: data.courseName, value: data.id } ];
            });
            courses = _.uniqBy(courses, "label");
            setCourses(courses)
        } else {
            return null;
        }
    }, [ EducationData ])

    const handleCourse = (value) => {
        setSelectedCourse(value)
        formik.setFieldValue('courseName', value.label)
        formik.setFieldValue('courseId', value.value)
    }

    const handlePassingYear = Date => {
        setDateShow(false);
        {
            Date !== undefined
                ? formik.setFieldValue('yearOfPassing', Date.getFullYear())
                : null
        }
    }

    return (
        <Form>
            <TextInput
                label='Enrollment No'
                placeholder='(Optional)'
                mode='flat'
                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                dense={ true }
                autoCapitalize='words'
                returnKeyType='next'
                onBlur={ formik.handleBlur('rollNumber') }
                onChangeText={ formik.handleChange('rollNumber') }
                value={ formik.values.rollNumber } />

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Course"
                    selectedValue={ selectedCourse }
                    onValueChange={ (value) => value !== '' ? handleCourse(value) : null } >
                    <Picker.Item label="Course" value='' />
                    { courses !== undefined
                        ? courses.map((data, index) => {
                            return <Picker.Item key={ index } label={ data.label } value={ data } />
                        })
                        : <Picker.Item label='Loading' value='' /> }
                </Picker>
            </Item>

            <Item last>
                <Text style={ { ...textStyles.regularText, ...styles.DatePickerText } } onPress={ () => setDateShow(true) } >
                    { formik.values.yearOfPassing !== '' ? formik.values.yearOfPassing : 'Year' }
                </Text>
                { dateShow
                    ? <DateTimePicker
                        minimumDate={ new Date(1950, 0) }
                        maximumDate={ new Date() }
                        value={ new Date(new Date().getFullYear(), new Date().getMonth()) }
                        mode='date'
                        display='default'
                        onChange={ (event, date) => handlePassingYear(date) }
                    />
                    : null }
            </Item>
            { formik.errors.courseName || formik.errors.yearOfPassing ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>Please Select all Course Details</Text> : null }

            <View style={ styles.buttonContainer }>
                <AppButton onPress={ formik.handleSubmit }>Add</AppButton>
                <AppButtonSec type='reset' onPress={ () => onReset() }>Cancel</AppButtonSec>
            </View>
        </Form>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },

    DatePickerText: {
        marginHorizontal: 10,
        marginVertical: 15,
        fontSize: 16
    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(ProfileEducationFormPhD);