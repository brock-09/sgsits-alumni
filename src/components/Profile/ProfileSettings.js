import React, { useState, useEffect } from 'react';
import { StyleSheet, Switch } from 'react-native';
import { View, CheckBox, Text, ListItem, Label, List, Toast } from 'native-base';
import { useFormik } from 'formik';
import { connect } from 'react-redux';
import { axiosall } from '../../util/axiosall';
import axios from 'axios';
import AppButton from '../AppButton';
import colors from '../../constants/colors';
import textStyles from '../../constants/textStyles';
import api from '../../constants/api';

const ProfileSettings = props => {

    const [accountInfo, setAccountInfo] = useState();

    const initialValues = {
        phoneVisible: false,
        emailVisble: false
    }

    useEffect(() => {
        const accountDetails = props.Auths[0].name;
        setAccountInfo(accountDetails);

        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: accountDetails.accountId }
        };

        axiosall('get', api.GET_SETTINGS, configuration)
            .then((response) => {
                const result = response.data;
                {
                    result[0] !== undefined
                        ? result.map(data => {
                            if (data.type === "phone") {
                                formik.setFieldValue('phoneVisible', data.isVisible)
                            } else if (data.type === "email") {
                                formik.setFieldValue('emailVisble', data.isVisible)
                            }
                        })
                        : (formik.setFieldValue('phoneVisible', 'false'), formik.setFieldValue('emailVisble', 'false'))
                }
            })
            .catch((error) => {
                console.log('Settings', error);
            })
    }, [])


    const onSubmit = values => {
        let params = [{
            accountId: accountInfo.accountId,
            type: "phone",
            isVisible: formik.values.phoneVisible
        }, {
            accountId: accountInfo.accountId,
            type: "email",
            isVisible: formik.values.emailVisble
        }]

        let configuration = {
            headers: {
                Authorization: "Bearer " + accountInfo.token
            }
        }

        axios.post(api.BASE + api.EDIT_SETTINGS, params, configuration)
            .then((response) => {
                Toast.show({
                    text: 'Profile Settings Updated',
                    buttonText: 'Okay',
                    buttonTextStyle: { color: '#ccc' },
                    textStyle: { ...textStyles.bodyText, color: colors.base },
                    style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
                    duration: 3000
                })
            })
            .catch((errors) => {
                console.log('Settigns', errors);
            })
    }

    const formik = useFormik({
        initialValues,
        onSubmit
    })

    return (
        <View style={styles.container} >
            <List>
                <ListItem style={styles.ItemBorderColor}>
                    <View>
                        <Text style={{ ...textStyles.regularText, ...styles.Margin }}
                            onPress={() => formik.setFieldValue('emailVisble', (formik.values.emailVisble ? false : true))}>
                            Hide email from other users
                        </Text>
                    </View>
                    <Switch
                        trackColor={{ false: '#f4f3f4', true: colors.primary }}
                        thumbColor={formik.values.emailVisble ? colors.primary : '#f4f3f4'}
                        onValueChange={() => formik.setFieldValue('emailVisble', (formik.values.emailVisble ? false : true))}
                        value={!formik.values.emailVisble} />
                </ListItem>


                <ListItem style={styles.ItemBorderColor}>
                    <View>
                        <Text style={{ ...textStyles.regularText, ...styles.Margin }}
                            onPress={() => formik.setFieldValue('phoneVisible', (formik.values.phoneVisible ? false : true))}>
                            Hide phone number from other users
                        </Text>
                    </View>
                    <Switch
                        trackColor={{ false: '#f4f3f4', true: colors.primary }}
                        thumbColor={formik.values.phoneVisible ? colors.primary : '#f4f3f4'}
                        onValueChange={() => formik.setFieldValue('phoneVisible', (formik.values.phoneVisible ? false : true))}
                        value={!formik.values.phoneVisible} />
                </ListItem>

                <View style={styles.ButtonContainer}>
                    <AppButton title='Save' type='submit' onPress={formik.handleSubmit}>Save</AppButton>
                </View>
            </List>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    ButtonContainer: {
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

    Margin: {
        marginHorizontal: 5,
        fontSize: 16
    },

    ItemBorderColor: {
        marginVertical: 0,
        borderColor: 'transparent',
        justifyContent: 'space-between'
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}


export default connect(mapStateToProps)(ProfileSettings);
