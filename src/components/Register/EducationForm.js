import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Form, Picker, Item, Text } from 'native-base';
import { TextInput } from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import _ from 'lodash';
import { connect } from 'react-redux';
import { addEducation } from '../../redux/actions/RegEducationalActions';
import { axiosall } from '../../util/axiosall';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import LoadingScreen from '../LoadingScreen';
import colors from '../../constants/colors';
import api from '../../constants/api';

const EducationForm = props => {

    const EducationData = props.data;

    const [ degrees, setdegrees ] = useState();
    const [ courses, setCourses ] = useState();
    const [ selectedCourse, setSelectedCourse ] = useState();
    const [ dateShow, setDateShow ] = useState(false);
    const [ show, setShow ] = useState(true);

    const initialValues = {
        type: props.type.toLowerCase(),
        collegeName: 'SGSITS',
        degree: '',
        courseId: '',
        courseName: '',
        rollNumber: '',
        yearOfPassing: '',
        isPartTime: '',
    }

    const validationSchema = Yup.object({
        rollNumber: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed'),
        degree: Yup.string().required('Required'),
        courseName: Yup.string().required('Required'),
        yearOfPassing: Yup.string().required('Required'),
        isPartTime: Yup.bool().required('Required'),
    })

    const onSubmit = values => {
        const data = {
            courseName: values.courseName,
            degree: values.degree,
            isPartTime: values.isPartTime
        }

        axiosall('get', api.VALIDATE_COURSE, { params: data })
            ?.then((response) => {
                if (response.data === true) {
                    setShow(false);
                    //Adding type again to override initialValues type
                    const data = {
                        ...formik.values,
                        type: props.type.toLowerCase()
                    }
                    props.addEducation(data)
                }
            })
            .catch((error) => {
                console.log('Error', error);
                Alert.alert(
                    '',
                    'Course Type is Invalid for Course: ' + values.courseName,
                    [ { text: 'ok' } ]
                )
            })
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })

    useEffect(() => {
        //To Resetform on switching between UG and PG
        formik.handleReset();
        setSelectedCourse();
        //Method to showOrhide addButton for Degree
        const educationData = props.RegEducational;
        educationData.map((item) => {
            if (props.type.toLowerCase() === item.type) {
                return setShow(false);
            }
            else {
                setShow(true)
            }
        })

        if (EducationData) {
            //Fetching Degree from Education Data
            let degrees = [];
            EducationData.map(data => {
                return degrees = [ ...degrees, { label: data.degree.trim(), value: data.degree.trim() } ];
            });
            degrees = _.uniqBy(degrees, "label");
            setdegrees(degrees)
        } else {
            return null;
        }
    }, [ EducationData, props ])

    const handleDegree = value => {
        formik.setFieldValue('degree', value);
        //Initializing Course Array by filtering EducationData
        const courseData = EducationData.filter(data => {
            return data.degree.includes(value)
        });
        //Fetching Courses dependent on the Degree
        if (courseData) {
            let courses = [];
            courseData.map(data => {
                return courses = [ ...courses, { label: data.courseName, value: data.id } ];
            });
            //Getting Unique Courses to avoid duplicacy
            courses = _.uniqBy(courses, "label");
            setCourses(courses)
        }
    }

    const handleCourse = (value) => {
        setSelectedCourse(value)
        formik.setFieldValue('courseName', value.label)
        formik.setFieldValue('courseId', value.value)
    }

    const handlePassingYear = Date => {
        setDateShow(false);
        {
            Date !== undefined
                ? formik.setFieldValue('yearOfPassing', Date.getFullYear())
                : null
        }
    }

    if (EducationData === undefined) {
        return <LoadingScreen />;
    }

    return (
        <Form>
            <TextInput
                label='Enrollment No'
                placeholder='(Optional)'
                mode='flat'
                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                dense={ true }
                autoCapitalize='words'
                returnKeyType='next'
                onBlur={ formik.handleBlur('rollNumber') }
                onChangeText={ formik.handleChange('rollNumber') }
                value={ formik.values.rollNumber } />

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Degree"
                    selectedValue={ formik.values.degree }
                    onValueChange={ (value) => value !== '' ? handleDegree(value) : null }>
                    <Picker.Item label="Degree" value='' />
                    { degrees !== undefined
                        ? degrees.map((data, index) => {
                            return <Picker.Item key={ index } label={ data.label } value={ data.value } />
                        })
                        : <Picker.Item label='Loading' value='' /> }
                </Picker>
            </Item>

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Course"
                    selectedValue={ selectedCourse }
                    onValueChange={ (value) => value !== '' ? handleCourse(value) : null } >
                    <Picker.Item label="Course" value='' />
                    { courses !== undefined
                        ? courses.map((data, index) => {
                            return <Picker.Item key={ index } label={ data.label } value={ data } />
                        })
                        : <Picker.Item label='Loading' value='' /> }
                </Picker>
            </Item>

            <Item last>
                <Picker
                    mode="dropdown"
                    placeholder="Course Type"
                    selectedValue={ formik.values.isPartTime }
                    onValueChange={ (value) => value !== '' ? formik.setFieldValue('isPartTime', value) : null }>
                    <Picker.Item label="Type" value='' />
                    <Picker.Item label="Full-Time" value={ false } />
                    <Picker.Item label="Part-Time" value={ true } />
                </Picker>
            </Item>

            <Item last>
                {/* <Label style={ textStyles.regularText }>Passing Year</Label> */ }
                <Text style={ { ...textStyles.regularText, ...styles.DatePickerText } } onPress={ () => setDateShow(true) } >
                    { formik.values.yearOfPassing !== '' ? formik.values.yearOfPassing : 'Year' }
                </Text>
                { dateShow
                    ? <DateTimePicker
                        minimumDate={ new Date(1950, 0) }
                        maximumDate={ new Date() }
                        value={ new Date(new Date().getFullYear(), new Date().getMonth()) }
                        mode='date'
                        display='default'
                        onChange={ (event, date) => handlePassingYear(date) }
                    />
                    : null }
            </Item>
            { formik.errors.degree || formik.errors.courseName || formik.errors.yearOfPassing || formik.errors.isPartTime ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>Please Select all Course Details</Text> : null }

            { show ? <View style={ styles.buttonContainer }>
                <AppButtonSec type='reset' onPress={ formik.handleReset }>Cancel</AppButtonSec>
                <AppButton onPress={ formik.handleSubmit }>Add</AppButton>
            </View> : null }
        </Form>

    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },

    DatePickerText: {
        marginHorizontal: 10,
        marginVertical: 15,
        fontSize: 16
    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    }
});

const mapStateToProps = (state) => {
    return {
        RegEducational: state.RegEducational.educational
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addEducation: (data) => {
            dispatch(addEducation(data));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EducationForm);