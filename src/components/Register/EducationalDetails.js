import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { Container, Content, CheckBox, Text, Card, Body, CardItem, Icon } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../../util/axiosall';
import { deleteEducation } from '../../redux/actions/RegEducationalActions';
import LoadingScreen from '../LoadingScreen';
import EducationForm from './EducationForm';
import EducationFormPhD from './EducationFormPhD';
import AppButton from '../AppButton';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import api from '../../constants/api';

const EducationalDetails = props => {

    const [ loading, setLoading ] = useState(false);
    const [ PGCourses, setPGCourses ] = useState();
    const [ UGCourses, setUGCourses ] = useState();
    const [ PHDCourses, setPHDCourses ] = useState();
    const [ type, setType ] = useState('');
    const [ EducationData, setEducationData ] = useState();

    const [ ugPresent, setugPresent ] = useState(false);
    const [ pgPresent, setpgPresent ] = useState(false);
    const [ phdPresent, setphdPresent ] = useState(false);

    useEffect(() => {
        setLoading(true)
        const educationdata = props.RegEducational.educational;
        setEducationData(educationdata);

        axiosall('get', api.GET_ALL_COURSES)
            ?.then((response) => {
                const result = response.data;
                setUGCourses(result.UG);
                setPGCourses(result.PG);
                setPHDCourses(result.PHD);
                setLoading(false)
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }, [ props ])

    const onHandleSubmit = () => {
        return props.navigation.navigate('ProfessionalDetails')
    }

    const EducationCard = itemData => {
        if (itemData.item.type === 'ug') {
            setugPresent(true);
        }
        else if (itemData.item.type === 'pg') {
            setpgPresent(true);
        }
        else if (itemData.item.type === 'phd') {
            setphdPresent(true);
        }

        const onDelete = key => {
            props.deleteEducation(key.courseId);
            return props.navigation.replace('EducationalDetails');
        }
        return (
            <Card>
                <CardItem>
                    <Body>
                        <Text style={ textStyles.regularText }>
                            { itemData.item.degree + ' - ' + itemData.item.courseName }{ itemData.item.isPartTme ? '(Part Time)' : '(Full Time)' }
                        </Text>
                        <Text style={ textStyles.bodyText }>
                            { itemData.item.yearOfPassing }
                        </Text>
                    </Body>
                    <Icon name='delete' type='MaterialCommunityIcons' style={ styles.Icons } onPress={ () => onDelete(itemData.item) } />
                </CardItem>
            </Card >
        );
    }

    if (loading && PGCourses === undefined && UGCourses === undefined && PHDCourses === undefined) {
        return <LoadingScreen />;
    }

    let formOutput;

    if (type == 'UG') {
        formOutput = (
            <View style={ styles.formContainer }>
                <Text style={ textStyles.regularText }>Add UG Course</Text>
                <EducationForm type={ type } data={ UGCourses } />
            </View>
        )
    }
    else if (type === 'PG') {
        formOutput = (
            <View style={ styles.formContainer }>
                <Text style={ textStyles.regularText }>Add PG Course</Text>
                <EducationForm type={ type } data={ PGCourses } />
            </View>
        )
    }
    else if (type == 'PHD') {
        formOutput = (
            <View style={ styles.formContainer }>
                <Text style={ textStyles.regularText }>Add PhD Course</Text>
                <EducationFormPhD type={ type } data={ PHDCourses } />
            </View>
        )
    }
    return (
        <Container>
            <Content>
                <Text style={ { ...textStyles.regularText, ...styles.title } } numberOfLines={ 2 }>Add Educational Qualification from SGSITS, Indore</Text>
                <View style={ styles.screen }>
                    <CheckBox
                        onPress={ () => setType('UG') }
                        disabled={ ugPresent }
                        checked={ type === 'UG' || ugPresent } color={ colors.primary } />
                    <Text>UG</Text>
                    <CheckBox
                        onPress={ () => setType('PG') }
                        disabled={ pgPresent }
                        checked={ type === 'PG' || pgPresent } color={ colors.primary } />
                    <Text>PG</Text>
                    <CheckBox
                        onPress={ () => setType('PHD') }
                        disabled={ phdPresent }
                        checked={ type === 'PHD' || phdPresent } color={ colors.primary } />
                    <Text>PhD</Text>
                </View>

                { formOutput }

                <View style={ styles.title }>
                    { EducationData !== undefined
                        ? <FlatList
                            keyExtractor={ (item, index) => index }
                            data={ EducationData }
                            renderItem={ EducationCard } />
                        : <Text style={ { ...textStyles.regularText, ...styles.Text } }>No Data Found</Text> }
                </View>

                { EducationData !== undefined
                    ? EducationData.length > 0
                        ? <View style={ styles.buttonContainer }>
                            <AppButton onPress={ () => onHandleSubmit() }>Next</AppButton>
                        </View>
                        : null
                    : null }
            </Content>
        </Container>
    );
};

const styles = StyleSheet.create({

    formContainer: {
        margin: 15,
        justifyContent: 'space-evenly',
    },

    buttonContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },

    screen: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },

    title: {
        margin: 10,
    },

    Icons: {
        color: colors.primary,
        marginHorizontal: 5,
    },

})

const mapStateToProps = (state) => {
    return {
        RegEducational: state.RegEducational
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteEducation: (key) => {
            dispatch(deleteEducation(key));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EducationalDetails);