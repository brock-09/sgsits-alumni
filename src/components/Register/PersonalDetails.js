import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Container, Content, Form, Item, Label, DatePicker, Radio, Input, ListItem, Picker } from 'native-base';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInput } from 'react-native-paper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { axiosall } from '../../util/axiosall';
import { connect } from 'react-redux';
import { addPersonal } from '../../redux/actions/RegPersonalActions';
import countryCodeList from '../../util/countryCodeList';
import AppButton from '../AppButton';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import moment from 'moment';
import api from '../../constants/api';

const PersonalDetails = props => {

    const [ Country, setCountry ] = useState();
    const [ selectedCountry, setSelectedCountry ] = useState();
    const [ State, setState ] = useState();
    const [ selectedState, setSelectedState ] = useState();
    const [ Cities, setCities ] = useState();
    const [ countryCode, setCountryCode ] = useState('+91');
    const [ dateShow, setDateShow ] = useState(false);

    useEffect(() => {
        axiosall('get', api.GET_COUNTRIES)
            ?.then((response) => {
                const result = response.data;
                setCountry(result.rows);
            })
            .catch((error) => {
                console.log(error);
            });
    }, [ props ]);


    const initialValues = {
        firstName: '',
        lastName: '',
        DOB: '',
        gender: '',
        phoneNumber: '',
        country: '',
        state: '',
        currentCity: '',
    }

    const capitalize = /^[A-Z][a-z0-9_-]{2,19}$/
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
    const validationSchema = Yup.object({
        firstName: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .matches(capitalize, 'First Character Should be Capital/UpperCase and Min. 3')
            .required('Required'),
        lastName: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .matches(capitalize, 'First Character Should be Capital/UpperCase and Min. 3')
            .required('Required'),
        phoneNumber: Yup.string()
            .matches(phoneRegExp, 'Phone Number Not Valid')
            .min(10, 'Min. characters 10')
            .required('Required'),
        DOB: Yup.string().required('Required'),
        gender: Yup.string().required('Required'),
        country: Yup.string().required('Required'),
        state: Yup.string().required('Required'),
    })

    const onSubmit = (values) => {
        let mobile = countryCode + formik.values.phoneNumber;
        const data = {
            ...formik.values,
            phoneNumber: mobile
        }
        //Adding Data to Reducer
        props.addPersonal(data)
        return props.navigation.navigate('EducationalDetails')
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })

    const handleCountry = value => {
        formik.setFieldValue('state', '')
        formik.setFieldValue('currentCity', '')
        setSelectedCountry(value)
        formik.setFieldValue('country', value.name)

        axiosall('get', `/getStatesByCountry?id=${ value.id }`)
            ?.then((response) => {
                const result = response.data
                setState(result.rows)
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }

    const handleState = value => {
        formik.setFieldValue('currentCity', '')
        setSelectedState(value)
        formik.setFieldValue('state', value.name)

        axiosall('get', `/getCitiesByState?id=${ value.id }`)
            ?.then((response) => {
                const result = response.data;
                setCities(result.rows);
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }

    return (
        <Container>
            <Content>
                <View>
                    <Text style={ { ...textStyles.regularText, ...styles.title } }>Add Personal Details </Text>
                    <Form style={ styles.FormContainer }>
                        <TextInput
                            label='First Name'
                            mode='flat'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                            dense={ true }
                            autoCapitalize='words'
                            returnKeyType='next'
                            onBlur={ formik.handleBlur('firstName') }
                            onChangeText={ formik.handleChange('firstName') }
                            value={ formik.values.firstName }
                            error={ formik.touched.firstName && formik.errors.firstName ? true : false } />
                        { formik.touched.firstName && formik.errors.firstName
                            ? <Text style={ styles.errors }>{ formik.errors.firstName }</Text>
                            : null }

                        <TextInput
                            label='Last Name'
                            mode='flat'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                            dense={ true }
                            autoCapitalize='words'
                            returnKeyType='next'
                            onBlur={ formik.handleBlur('lastName') }
                            onChangeText={ formik.handleChange('lastName') }
                            value={ formik.values.lastName }
                            error={ formik.touched.lastName && formik.errors.lastName ? true : false } />
                        { formik.touched.lastName && formik.errors.lastName
                            ? <Text style={ styles.errors }>{ formik.errors.lastName }</Text>
                            : null }

                        <Item last>
                            <Label style={ textStyles.bodyText }>Gender</Label>
                            <ListItem style={ styles.ItemBorderColor }>
                                <Radio
                                    onPress={ () => formik.setFieldValue('gender', 'Male') }
                                    selected={ formik.values.gender === 'Male' }
                                />
                                <Text style={ styles.checkboxMargin }>Male</Text>
                            </ListItem>
                            <ListItem style={ styles.ItemBorderColor }>
                                <Radio
                                    onPress={ () => formik.setFieldValue('gender', 'Female') }
                                    selected={ formik.values.gender === 'Female' }
                                />
                                <Text style={ styles.checkboxMargin }>Female</Text>
                            </ListItem>
                        </Item>

                        <Item last>
                            <Label style={ { ...textStyles.bodyText, ...styles.Date } }>Date of Birth</Label>
                            <Text style={ { ...textStyles.bodyText, ...styles.DatePickerText } } onPress={ () => setDateShow(true) } >
                                { formik.values.DOB !== '' && formik.values.DOB !== undefined ? moment(formik.values.DOB).format('DD/MM/YYYY') : 'Date Of Birth' }
                            </Text>
                            { dateShow
                                ? <DateTimePicker
                                    mode='date'
                                    display='default'
                                    minimumDate={ new Date(1950, 0) }
                                    maximumDate={ new Date(new Date().getFullYear() - 18, new Date().getMonth()) }
                                    value={ new Date(new Date().getFullYear() - 18, new Date().getMonth()) }
                                    onChange={ (event, date) => (setDateShow(false), formik.setFieldValue('DOB', date)) } />
                                : null }
                        </Item>

                        <Item last>
                            <Picker
                                mode="dropdown"
                                placeholder="drop"
                                style={ { width: '40%' } }
                                selectedValue={ countryCode }
                                onValueChange={ (value) => value !== '' ? setCountryCode(value) : null }>
                                <Picker.Item label="Code" value='' />
                                { countryCodeList !== undefined
                                    ? countryCodeList.map((data, index) => {
                                        return <Picker.Item key={ index } label={ data.label + ' ' + data.value } value={ data.value } />
                                    })
                                    : <Picker.Item label='Loading' value='' /> }
                            </Picker>
                            <TextInput
                                style={ { width: '60%', marginBottom: -2 } }
                                label='Mobile No'
                                mode='flat'
                                underlineColor='transparent'
                                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                                dense={ true }
                                keyboardType="numeric"
                                returnKeyType='next'
                                maxLength={ 10 }
                                onBlur={ formik.handleBlur('phoneNumber') }
                                onChangeText={ formik.handleChange('phoneNumber') }
                                value={ formik.values.phoneNumber }
                                error={ formik.touched.phoneNumber && formik.errors.phoneNumber ? true : false } />
                        </Item>
                        { formik.touched.phoneNumber && formik.errors.phoneNumber
                            ? <Text style={ styles.errors }>{ formik.errors.phoneNumber }</Text>
                            : null }

                        <Item last>
                            <Picker
                                mode="dropdown"
                                placeholder="Country"
                                selectedValue={ selectedCountry }
                                onValueChange={ (value) => value !== '' ? handleCountry(value) : null }>
                                <Picker.Item label="Country" value='' />
                                { Country !== undefined
                                    ? Country.map((data, index) => {
                                        return <Picker.Item key={ index } label={ data.name } value={ data } />
                                    })
                                    : <Picker.Item label='Loading' value='' /> }
                            </Picker>
                        </Item>

                        <Item last>
                            <Picker
                                mode="dropdown"
                                placeholder="State"
                                selectedValue={ selectedState }
                                onValueChange={ (value) => value !== '' ? handleState(value) : null }>
                                <Picker.Item label="State" value='' />
                                { State !== undefined
                                    ? State.map((data, index) => {
                                        return <Picker.Item key={ index } label={ data.name } value={ data } />
                                    })
                                    : <Picker.Item label='Please Select Country' value='' /> }
                            </Picker>
                        </Item>

                        <Item last>
                            <Picker
                                mode="dropdown"
                                placeholder="City"
                                selectedValue={ formik.values.currentCity }
                                onValueChange={ (value) => value !== '' ? formik.setFieldValue('currentCity', value) : null }>
                                <Picker.Item label="City" value='' />
                                { Cities !== undefined
                                    ? Cities.map((data, index) => {
                                        return <Picker.Item key={ index } label={ data.name } value={ data.name } />
                                    })
                                    : <Picker.Item label='City' value='' /> }
                            </Picker>
                        </Item>

                        { formik.errors.gender || formik.errors.state || formik.errors.country || formik.errors.DOB
                            ? <Text style={ styles.errors }>Please Fill all Details</Text>
                            : null }

                        <View style={ styles.buttonContainer }>
                            <AppButton type='submit' onPress={ formik.handleSubmit }>Next</AppButton>
                        </View>
                    </Form>
                </View>
            </Content>
        </Container >
    )

}

const styles = StyleSheet.create({

    FormContainer: {
        marginHorizontal: 10,
    },

    errors: {
        fontSize: 12,
        color: 'red',
        marginLeft: 15,
        margin: 5
    },

    buttonContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },

    title: {
        margin: 10
    },

    checkboxMargin: {
        marginHorizontal: 10
    },

    ItemBorderColor: {
        marginVertical: 0,
        borderColor: 'transparent',
    },

    DatePickerText: {
        margin: 13
    },

    Date: {
        marginVertical: 16
    },
})

const mapStateToProps = (state) => {
    return {
        RegPersonal: state.RegPersonal.personal
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPersonal: (data) => {
            dispatch(addPersonal(data));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetails);