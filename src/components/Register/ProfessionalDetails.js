import React, { useState, useEffect } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { Container, Content, Icon, Form, Text, Item, Label, Card, CardItem, Body, DatePicker, Picker, CheckBox, Toast } from "native-base";
import DateTimePicker from '@react-native-community/datetimepicker';
import { TextInput } from 'react-native-paper';
import { connect, useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import moment from 'moment';
import { addProfessional, deleteProfessional, deleteAllProfessional } from '../../redux/actions/RegProfessionalActions';
import { deleteAllEducation } from '../../redux/actions/RegEducationalActions';
import { deletePersonal } from '../../redux/actions/RegPersonalActions';
import { deleteLogin } from '../../redux/actions/AuthActions';
import AppButton from '../AppButton';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import api from '../../constants/api';

const ProfessionalDetails = props => {

    const [ startDate, setStartDate ] = useState(new Date());
    const [ ProfessionalData, setProfessionalData ] = useState([]);
    const [ dateShow, setDateShow ] = useState(false);

    const dispatch = useDispatch();
    const { account } = useSelector((state) => state.AuthReducer);

    useEffect(() => {
        const ProfessionalDetails = props.RegProfessional;
        setProfessionalData(ProfessionalDetails);
    }, [ props ])

    const initialValues = {
        id: Math.random(),
        title: '',
        companyName: '',
        location: '',
        type: '',
        monthOfJoining: '',
        yearOfJoining: '',
        monthOfLeaving: '',
        yearOfLeaving: '',
        isWorking: false
    };

    const onSubmit = values => {
        props.addProfessional(values)
        formik.handleReset()
    }

    const ToastMessage = message => {
        Toast.show({
            text: message,
            buttonText: 'Okay',
            buttonTextStyle: { color: '#ccc' },
            textStyle: { ...textStyles.bodyText, color: colors.base },
            style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
            duration: 3000
        })
    }

    const handleOnSubmit = values => {
        const accountDetails = props.Auths[ 0 ].name;
        const PersonalDetails = props.RegPersonal.data;
        const EducationDetails = props.RegEducational;
        const ProfessionalDetails = props.RegProfessional;

        let RegisterData = {
            accountId: accountDetails.accountId,
            personal: PersonalDetails,
            education: EducationDetails,
            professional: ProfessionalDetails
        };

        let params = {
            url: api.BASE + api.REGISTER_PROFILE,
            configuration: {
                headers: {
                    'Authorization': "Bearer " + accountDetails.token
                }
            },
            data: RegisterData
        }

        axios.post(params.url, params.data, params.configuration)
            .then((response) => {
                if (response.data.msg === "User information successfully updated.") {
                    const key = account[ 0 ].key;

                    dispatch(deletePersonal());
                    dispatch(deleteAllProfessional());
                    dispatch(deleteAllEducation());
                    dispatch(deleteLogin(key));
                    ToastMessage(response.data.msg);
                }
            })
            .catch((error) => {
                console.log('Error', error);
            })
    };

    const validationSchema = Yup.object({
        title: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed').
            required('Required'),
        companyName: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .required('Required'),
        location: Yup.string()
            .strict(true)
            .trim('Whitespaces not allowed')
            .required('Required'),
        type: Yup.string().required('Required'),
        monthOfJoining: Yup.string().required('Required'),
        monthOfLeaving: Yup.string()
            .when('isWorking', {
                is: false,
                otherwise: Yup.string().notRequired(),
                then: Yup.string().required('Required')
            }),

    });

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    });

    const handleStartDate = Date => {
        setStartDate(Date);
        const monthNames = [ "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December" ];
        formik.setFieldValue('monthOfJoining', monthNames[ Date.getMonth() ]);
        formik.setFieldValue('yearOfJoining', Date.getFullYear());
    }

    const handleEndDate = Date => {
        const monthNames = [ "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December" ];
        formik.setFieldValue('monthOfLeaving', monthNames[ Date.getMonth() ]);
        formik.setFieldValue('yearOfLeaving', Date.getFullYear());
    }


    const ProfessionalCard = result => {

        const onDelete = key => {
            console.log(key.id);
            props.deleteProfessional(key.id);
        }

        return (
            <Card>
                <CardItem >
                    <Body>
                        <Text style={ textStyles.regularText }>
                            { result.item.companyName }
                        </Text>
                        <Text style={ textStyles.bodyText }>
                            { result.item.title + ' ' + '(' + result.item.type + ')' }
                        </Text>
                        <Text style={ textStyles.bodyText }>
                            { result.item.location }
                        </Text>
                        <Text style={ textStyles.bodyText }>
                            { result.item.monthOfJoining + '-' + result.item.yearOfJoining + ' ' + 'to' + ' ' }{ result.item.isWorking ? 'Present' : result.item.monthOfLeaving + '-' + result.item.yearOfLeaving }
                        </Text>
                    </Body>
                    <Icon name='delete' type='MaterialCommunityIcons' style={ styles.deleteIcon } onPress={ () => onDelete(result.item) } />
                </CardItem>
            </Card>
        );
    };

    return (
        <Container>
            <Content>
                <View>
                    <Text style={ { ...textStyles.regularText, ...styles.title } }>Add Professional Details</Text>
                    <View style={ styles.formContainer }>
                        <Form>
                            <TextInput
                                label='Job Title'
                                mode='flat'
                                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                                dense={ true }
                                autoCapitalize='words'
                                returnKeyType='next'
                                onBlur={ formik.handleBlur('title') }
                                onChangeText={ formik.handleChange('title') }
                                value={ formik.values.title } />
                            { formik.touched.title && formik.errors.title
                                ? <Text style={ styles.errors }>{ formik.errors.title }</Text>
                                : null }

                            <TextInput
                                label='Company Name'
                                mode='flat'
                                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                                dense={ true }
                                autoCapitalize='words'
                                returnKeyType='next'
                                onBlur={ formik.handleBlur('companyName') }
                                onChangeText={ formik.handleChange('companyName') }
                                value={ formik.values.companyName } />
                            { formik.touched.companyName && formik.errors.companyName
                                ? <Text style={ styles.errors }>{ formik.errors.companyName }</Text>
                                : null }

                            <TextInput
                                label='Location'
                                mode='flat'
                                theme={ { colors: { placeholder: colors.primary, primary: colors.primary, background: 'transparent' } } }
                                dense={ true }
                                autoCapitalize='words'
                                returnKeyType='next'
                                onBlur={ formik.handleBlur('location') }
                                onChangeText={ formik.handleChange('location') }
                                value={ formik.values.location } />
                            { formik.touched.location && formik.errors.location
                                ? <Text style={ styles.errors }>{ formik.errors.location }</Text>
                                : null }

                            <Item last>
                                <Picker
                                    mode="dropdown"
                                    placeholder="Type"
                                    selectedValue={ formik.values.type }
                                    onValueChange={ (value) => value !== '' ? formik.setFieldValue('type', value) : null }>
                                    <Picker.Item label="Type" value='' />
                                    <Picker.Item label="Part-Time" value="Part-Time" />
                                    <Picker.Item label="Full-Time" value="Full-Time" />
                                    <Picker.Item label="Contract" value="Contract" />
                                </Picker>
                            </Item>

                            <Item last>
                                <Label style={ { ...textStyles.bodyText, ...styles.Date } }>From</Label>
                                <DatePicker
                                    minimumDate={ new Date(1950, 0) }
                                    maximumDate={ new Date() }
                                    locale={ "en" }
                                    modalTransparent={ true }
                                    animationType={ "fade" }
                                    androidMode={ "default" }
                                    placeHolderText="Date"
                                    placeHolderTextStyle={ { color: "#d3d3d3" } }
                                    onDateChange={ date => { handleStartDate(date) } }
                                    disabled={ false } />
                            </Item>

                            <Item last>
                                <Label style={ { ...textStyles.bodyText, ...styles.Date } }>To</Label>
                                { formik.values.isWorking != true
                                    ? <DatePicker
                                        minimumDate={ new Date(startDate) }
                                        maximumDate={ new Date() }
                                        locale={ "en" }
                                        modalTransparent={ true }
                                        animationType={ "fade" }
                                        androidMode={ "default" }
                                        placeHolderText="Date"
                                        placeHolderTextStyle={ { color: "#d3d3d3" } }
                                        onDateChange={ date => { handleEndDate(date) } }
                                        disabled={ false } />
                                    : null }

                                <CheckBox
                                    onPress={ () => formik.setFieldValue('isWorking', (formik.values.isWorking ? false : true)) }
                                    checked={ formik.values.isWorking === true }
                                    color={ colors.primary } />
                                <Text
                                    style={ styles.checkboxText }
                                    onPress={ () => formik.setFieldValue('isWorking', (formik.values.isWorking ? false : true)) }>
                                    Currently working here
                                </Text>
                            </Item>
                            { formik.errors.monthOfJoining || formik.errors.monthOfLeaving ? <Text style={ styles.errors }>{ 'Fill all Details' }</Text> : null }
                        </Form>

                        { formik.touched.title ? <View style={ styles.FormButtonContainer }>
                            <AppButtonSec type='reset' onPress={ formik.handleReset }>Cancel</AppButtonSec>
                            <AppButton onPress={ formik.handleSubmit }>Add</AppButton>
                        </View> : null }
                    </View>

                    <View style={ styles.title }>
                        { ProfessionalData !== undefined
                            ? <FlatList
                                keyExtractor={ (item, index) => index }
                                data={ ProfessionalData }
                                renderItem={ ProfessionalCard }
                            />
                            : null }
                    </View>

                    { !formik.touched.title ? <View style={ styles.buttonContainer }>
                        <AppButton onPress={ handleOnSubmit }>Submit</AppButton>
                    </View> : null }
                </View>
            </Content>
        </Container >
    )
}
const styles = StyleSheet.create({

    formContainer: {
        marginHorizontal: 10
    },

    buttonContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },

    FormButtonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },

    deleteIcon: {
        color: colors.primary,
        marginHorizontal: 5,
    },

    title: {
        margin: 10,
    },

    checkboxText: {
        marginHorizontal: 20,
    },

    Date: {
        marginVertical: 16,
    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    },
})


const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account,
        RegPersonal: state.RegPersonal.personal,
        RegProfessional: state.RegProfessional.professional,
        RegEducational: state.RegEducational.educational
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProfessional: (data) => {
            dispatch(addProfessional(data));
        },
        deleteProfessional: (key) => {
            dispatch(deleteProfessional(key));
        },
        deleteAllProfessional: () => {
            dispatch(deleteAllProfessional());
        },
        deleteAllEducation: () => {
            dispatch(deleteAllEducation());
        },
        deletePersonal: () => {
            dispatch(deletePersonal());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfessionalDetails);