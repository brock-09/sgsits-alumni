import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { Header, Item, Input, Icon } from 'native-base';
import colors from '../constants/colors';


const SearchHeader = (props) => {

  const [ searchData, setSearchData ] = useState('');

  const onSearch = (val) => {
    setSearchData(val);
    if (searchData != '' && searchData != ' ' && searchData != undefined) {
      const data = searchData
      props.onSearch(data.toLowerCase());
    }
    else {
      props.onSearch('');
    }
  }

  const onClear = () => {
    setSearchData('');
    props.onClear();
  }

  return (
    <Header
      searchBar
      rounded
      androidStatusBarColor={ colors.primary }
      iosBarStyle='light-content'
      style={ styles.header }>

      <Item>
        <Input
          placeholder='Search'
          onChangeText={ (val) => { onSearch(val) } }
          value={ searchData }
        />

        <Icon name='search' onPress={ () => onSearch(searchData) } />

        <Icon name='cross' type='Entypo' onPress={ () => onClear() } />

      </Item>
    </Header>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.primary,
  },
});

export default SearchHeader;

