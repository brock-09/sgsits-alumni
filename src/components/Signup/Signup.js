import React from 'react';
import { StyleSheet, View, Image, Dimensions, Linking } from 'react-native';
import { Container, Content, Form, Item, Label, CheckBox, Text, Toast } from 'native-base';
import { TextInput } from 'react-native-paper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import AppButton from '../AppButton';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import AppButtonSec from '../AppButtonSec';
import images from '../../constants/images';
import { axiosall } from '../../util/axiosall';
import api from '../../constants/api';

const Signup = props => {

    const initialValues = {
        email: '',
        password: '',
        confirmpassword: '',
        privacyPolicy: false
    }

    const ToastMessage = message => {
        Toast.show({
            text: message,
            buttonText: 'Okay',
            buttonTextStyle: { color: '#ccc' },
            textStyle: { ...textStyles.bodyText, color: colors.base },
            style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
            duration: 3000
        })
    }

    const onSubmit = values => {
        if (!formik.errors.email && !formik.errors.password) {
            axiosall('post', api.SIGNUP, values)
                ?.then((response) => {
                    const result = response.data;
                    ToastMessage(result.msg);
                    return props.navigation.navigate('VerifyEmail');
                })
                .catch(({ ...error }) => {
                    if (error && error.response.data.msg) {
                        ToastMessage(error.response.data.msg);
                    } else {
                        ToastMessage('Invalid Request');
                    }
                })
        }
    }

    const validationSchema = Yup.object({
        email: Yup.string()
            .trim('Whitespaces not allowed')
            .email('Invalid Format')
            .required('Required'),
        password: Yup.string()
            .trim('Whitespaces not allowed')
            .min(6, 'Min. characters 6')
            .required('Required'),
        confirmpassword: Yup.string()
            .oneOf([ Yup.ref('password'), null ], 'Passwords do not match')
            .required('Required'),
        privacyPolicy: Yup.bool()
            .oneOf([ true ], 'Please Read and Accept Privacy Policy')
    })

    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    });

    return (
        <Container>
            <Content contentContainerStyle={ styles.Content }>
                <View style={ styles.imageContainer }>
                    <Image source={ images.SGSITS_LOGO } style={ styles.bgImage } />
                </View>
                <View style={ styles.formContainer }>
                    <Form>
                        <TextInput
                            label='Email'
                            mode='outlined'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary } } }
                            keyboardType='email-address'
                            autoCapitalize='none'
                            returnKeyType='next'
                            autoCompleteType='email'
                            textContentType='emailAddress'
                            onBlur={ formik.handleBlur('email') }
                            onChangeText={ formik.handleChange('email') }
                            value={ formik.values.email }
                            error={ formik.touched.email && formik.errors.email ? true : false } />
                        { formik.touched.email && formik.errors.email
                            ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.email }</Text>
                            : null }

                        <TextInput
                            label='Password'
                            mode='outlined'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary } } }
                            secureTextEntry={ true }
                            returnKeyType='done'
                            onBlur={ formik.handleBlur('password') }
                            onChangeText={ formik.handleChange('password') }
                            value={ formik.values.password }
                            error={ formik.touched.password && formik.errors.password ? true : false } />
                        { formik.touched.password && formik.errors.password
                            ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.password }</Text>
                            : null }

                        <TextInput
                            label='Confirm Password'
                            mode='outlined'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary } } }
                            secureTextEntry={ true }
                            returnKeyType='done'
                            onBlur={ formik.handleBlur('confirmpassword') }
                            onChangeText={ formik.handleChange('confirmpassword') }
                            value={ formik.values.confirmpassword }
                            error={ formik.touched.confirmpassword && formik.errors.confirmpassword ? true : false } />
                        { formik.touched.confirmpassword && formik.errors.confirmpassword
                            ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.confirmpassword }</Text>
                            : null }

                        <Item style={ styles.ItemBorderColor }>
                            <CheckBox
                                onPress={ () => formik.setFieldValue('privacyPolicy', (formik.values.privacyPolicy ? false : true)) }
                                checked={ formik.values.privacyPolicy === true }
                                onBlur={ formik.handleBlur.privacyPolicy }
                                color={ colors.primary } />
                            <Label style={ { ...textStyles.bodyText, ...styles.checkboxText } }
                                onPress={ () => formik.setFieldValue('privacyPolicy', (formik.values.privacyPolicy ? false : true)) }>
                                I acknowledge and agree to the
                            </Label>
                            <Text
                                style={ { ...textStyles.bodyText, ...styles.TextLink } }
                                onPress={ () => Linking.openURL('http://staging.sgsitsalumni.in/privacy-policy') }>
                                Privacy Policy.
                            </Text>
                        </Item>
                        { formik.touched.privacyPolicy && formik.errors.privacyPolicy
                            ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.privacyPolicy }</Text>
                            : null }

                        <View style={ styles.buttonContainer }>
                            <AppButton title='Sign Up' type='submit' onPress={ formik.handleSubmit }>Sign Up</AppButton>
                        </View>
                    </Form>
                    <View style={ styles.SigninLink }>
                        <Text style={ textStyles.bodyText }>Already have an Account?</Text>
                        <View style={ styles.buttonLinkContainer }>
                            <AppButtonSec title='Sign In' onPress={ () => props.navigation.navigate('SignIn') }>Sign In</AppButtonSec>
                        </View>
                    </View>
                </View>
            </Content>
        </Container >
    );
};
const styles = StyleSheet.create({
    Content: {
        flex: 1,
        justifyContent: 'center',
    },

    imageContainer: {
        borderRadius: Math.floor(Dimensions.get('window').width * 0.7 / 2),
        overflow: 'hidden',
        alignItems: 'center',
    },

    bgImage: {
        height: 80,
        width: 80,
    },

    formContainer: {
        margin: 10,
    },

    buttonContainer: {
        margin: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    buttonLinkContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    },

    SigninLink: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    TextLink: {
        color: colors.primary,
    },

    ItemBorderColor: {
        borderColor: '#fff',
        marginLeft: 0,
    },

    checkboxText: {
        margin: 10,
        marginHorizontal: 20,
        marginRight: 0,
    },
});

export default Signup;