import React from 'react';
import { Image, StyleSheet, Dimensions } from 'react-native';
import { View, Text } from 'native-base';
import AppButtonSec from '../AppButtonSec';
import textStyles from '../../constants/textStyles';
import colors from '../../constants/colors';
import images from '../../constants/images';

const VerifyEmail = props => {
    return (
        <View style={ styles.container }>
            <View style={ styles.imageContainer }>
                <Image
                    style={ styles.imageStyle }
                    source={ images.SGSITS_LOGO } />
            </View>
            <View style={ styles.textContainer }>
                <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>
                    Verification Link has been sent to your Email
                </Text>
                <Text style={ { ...textStyles.bodyText, ...styles.textStyle } }>
                    Please click on the link that has just been sent to your email account to verify your email and Sign In to continue the registration process.
                </Text>
                <View style={ styles.buttonLinkContainer }>
                    <AppButtonSec title='Sign In' onPress={ () => props.navigation.navigate('SignIn') }>Sign In</AppButtonSec>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
    },

    imageContainer: {
        borderRadius: Math.floor(Dimensions.get('window').width * 0.7 / 2),
        overflow: 'hidden',
        alignItems: 'center',
        margin: 10,
    },

    imageStyle: {
        width: 80,
        height: 80,
    },

    textContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    textStyle: {
        margin: 5,
        textAlign: 'center',
    },

    buttonLinkContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    TextLink: {
        margin: 10,
        fontSize: 18,
        color: colors.primary,
    },
})

export default VerifyEmail;
