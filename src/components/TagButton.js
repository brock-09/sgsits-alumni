import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Text } from 'native-base';
import colors from '../constants/colors';

const TagButton = props => {
    return (
        <Button rounded style={ { ...styles.tagButton, ...props.style } } { ...props }>
            <Text style={ { ...styles.tagText, ...props.style } } numberOfLines={ 1 }> { (props.children) || (props.title) } </Text>
        </Button>
    );
};

const styles = StyleSheet.create({

    tagButton: {
        width: '48%',
        backgroundColor: colors.base,
        borderColor: '#ccc',
        borderWidth: 1,
        justifyContent: 'center',
    },

    tagText: {
        color: '#838d92',
        fontSize: 12,
    },

});

export default TagButton;