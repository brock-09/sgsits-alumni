import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList, ScrollView } from 'react-native';
import { View, Text, Card, CardItem, Body } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../../util/axiosall';
import textStyles from '../../constants/textStyles';
import LoadingScreen from '../LoadingScreen';
import ConnectionCardItem from '../ConnectionCardItem';
import colors from '../../constants/colors';
import api from '../../constants/api';

const UserProfileConnection = props => {

    const [ loading, setLoading ] = useState(false);
    const [ connections, setConnections ] = useState({
        confirmRequest: []
    });

    useEffect(() => {
        setLoading(true);
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;
        const viewId = props.viewId;

        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: viewId }
        };


        axiosall('get', api.GET_CONNECTION, configuration)
            ?.then((response) => {
                //Only Accepting Confirmed Connections
                let confirmRequest = [];
                response.data.forEach(element => {
                    if (element.status === "accepted") {
                        confirmRequest.push(element)
                    }
                });
                setConnections({ confirmRequest })
                setLoading(false);
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }, []);

    if (loading) {
        return <LoadingScreen />;
    }

    const confirmRequestCards = result => {
        return (<ConnectionCardItem data={ result.item } />)
    }

    return (
        <ScrollView>
            <View style={ styles.container }>
                <Card>
                    <CardItem header bordered>
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Connections</Text>
                    </CardItem>
                    <View style={ styles.CardsContainer }>
                        { connections.confirmRequest[ 0 ] !== undefined
                            ? <FlatList
                                keyExtractor={ (item, index) => item.id }
                                data={ connections.confirmRequest }
                                renderItem={ confirmRequestCards }
                                numColumns={ 2 }
                            />
                            : <Text style={ { ...textStyles.bodyText } }> No Connection Available</Text> }
                    </View>

                </Card>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        marginVertical: 10,
    },

    textStyle: {
        fontSize: 16,
        color: colors.primary
    },

    CardsContainer: {
        margin: 5
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(UserProfileConnection);