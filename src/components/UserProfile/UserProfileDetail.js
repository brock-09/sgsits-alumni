import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, ScrollView, Linking } from 'react-native';
import { Card, CardItem, Body, Icon } from 'native-base';
import HTML from 'react-native-render-html';
import colors from '../../constants/colors';
import textStyles from '../../constants/textStyles';
import LoadingScreen from '../LoadingScreen';
import AppButton from '../AppButton';

const UserProfileDetail = props => {

    const [ UserDetailData, setUserDetailData ] = useState();
    const [ EducatioData, setEducationData ] = useState();
    const [ ProfessionalData, setProfessionalData ] = useState();
    const [ phoneIsVisible, setPhoneIsVisible ] = useState(false);
    const [ emailIsVisible, setEmailIsVisible ] = useState(false);

    useEffect(() => {
        if (props.data !== undefined) {
            setUserDetailData(props.data.profile);
            setEducationData(props.data.education);
            setProfessionalData(props.data.professional);
            const settings = props.data.settings;
            {
                settings[ 0 ] !== undefined
                    ? settings.map(data => {
                        if (data.type === "phone") {
                            setPhoneIsVisible(data.isVisible)
                        }
                        if (data.type === "email") {
                            setEmailIsVisible(data.isVisible)
                        }
                    })
                    : (setPhoneIsVisible(false), setEmailIsVisible(false))
            }
        }
    }, [ props ]);

    if (UserDetailData === undefined || EducatioData === undefined || ProfessionalData === undefined) {
        return <LoadingScreen />;
    }

    const CONNECT_STATUS = props.status === 'accepted' ? 'Disconnect' : props.status;

    return (
        <ScrollView>
            <View style={ styles.PersonalDetailContainer }>
                { UserDetailData.firstName || UserDetailData.lastName
                    ? <View style={ styles.DetailContainer }>
                        <Text style={ { ...textStyles.regularText, ...styles.Name } }>
                            { UserDetailData.firstName + " " + UserDetailData.lastName }
                        </Text>
                    </View>
                    : null }

                { UserDetailData.email && emailIsVisible ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Email</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.email }</Text>
                </View> : null }
                { UserDetailData.gender ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Gender</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.gender }</Text>
                </View> : null }
                { UserDetailData.dateOfBirth ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Date of Birth</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ new Date(UserDetailData.dateOfBirth).toDateString() }</Text>
                </View> : null }
                { UserDetailData.phoneNumber && phoneIsVisible ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Mobile</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.phoneNumber }</Text>
                </View> : null }
                { UserDetailData.country ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Country</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.country }</Text>
                </View> : null }
                { UserDetailData.state ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>State</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.state }</Text>
                </View> : null }
                { UserDetailData.currentCity ? <View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>City</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.currentCity }</Text>
                </View> : null }
                { UserDetailData.userCode ? < View style={ styles.DetailContainer }>
                    <Text style={ { ...textStyles.bodyText, ...styles.DText } }>Alumni Unique Id</Text>
                    <Text style={ { ...textStyles.bodyText, ...styles.Dtext } }>{ UserDetailData.userCode }</Text>
                </View> : null }

                <View style={ styles.SocialLinksContainer }>
                    { UserDetailData.instagram ? <View><Icon name='instagram' type='FontAwesome' style={ { color: colors.instagram } } onPress={ () => Linking.openURL(UserDetailData.instagram) } /></View> : null }
                    { UserDetailData.facebook ? <View><Icon name='facebook-square' type='FontAwesome' style={ { color: colors.facebook } } onPress={ () => Linking.openURL(UserDetailData.facebook) } /></View> : null }
                    { UserDetailData.twitter ? <View><Icon name='twitter-square' type='FontAwesome' style={ { color: colors.twitter } } onPress={ () => Linking.openURL(UserDetailData.twitter) } /></View> : null }
                    { UserDetailData.linkedIn ? <View><Icon name='linkedin-square' type='FontAwesome' style={ { color: colors.linkdein } } onPress={ () => Linking.openURL(UserDetailData.linkedIn) } /></View> : null }
                    { UserDetailData.website ? <View><Icon name='web' type='MaterialCommunityIcons' onPress={ () => Linking.openURL(UserDetailData.website) } /></View> : null }
                </View>
            </View>

            <View style={ { justifyContent: 'center', alignItems: 'center' } }>
                <AppButton
                    small
                    onPress={ () => props.onSend() }
                    disabled={ CONNECT_STATUS === 'Disconnect' || CONNECT_STATUS === 'Pending' }>
                    { (CONNECT_STATUS !== 'Pending' && CONNECT_STATUS !== 'Disconnect') ? 'Connect' : CONNECT_STATUS }
                </AppButton>
            </View>

            <View style={ styles.CardContainer }>
                <Card>
                    <CardItem header bordered>
                        <Icon name='file-text' type='Feather' style={ { color: colors.primary } } />
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>
                            About
                        </Text>
                    </CardItem>
                    <CardItem>
                        <Body>
                            <HTML html={ UserDetailData.aboutMe ? UserDetailData.aboutMe : '<p>Write something about yourself...</p>' } tagsStyles={ { p: textStyles.bodyText } } />
                        </Body>
                    </CardItem>
                </Card>

                <Card>
                    <CardItem header bordered>
                        <Icon name='university' type='FontAwesome' style={ { color: colors.primary } } />
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Education</Text>
                    </CardItem>
                    { EducatioData[ 0 ] !== undefined ? EducatioData.map((data, index) =>
                        <CardItem key={ data.educationId }>
                            <Body>
                                { index === 0
                                    ? <Text style={ { ...textStyles.bodyText, fontSize: 8 } }>
                                        From Shri Govindram Seksaria Institute of Technology and Science, Indore
                                    </Text>
                                    : null }
                                <Text style={ textStyles.bodyText }>
                                    { data.degree + ' ' + data.courseName }{ data.isPartTme ? '(Part Time)' : '(Full Time)' }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.yearOfPassing }
                                </Text>
                            </Body>
                        </CardItem>
                    ) : <CardItem><Body><Text style={ textStyles.bodyText }>No Data</Text></Body></CardItem> }
                </Card>

                <Card>
                    <CardItem header bordered>
                        <Icon name='building' type='FontAwesome' style={ { color: colors.primary } } />
                        <Text style={ { ...textStyles.regularText, ...styles.textStyle } }>Experience</Text>
                    </CardItem>
                    { ProfessionalData[ 0 ] !== undefined ? ProfessionalData.map(data =>
                        <CardItem key={ data.id }>
                            <Body>
                                <Text style={ textStyles.regularText }>
                                    { data.companyName }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.title + ' ' + '(' + data.type + ')' }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.location }
                                </Text>
                                <Text style={ textStyles.bodyText }>
                                    { data.monthOfJoining + '-' + data.yearOfJoining + ' ' + 'to' + ' ' }{ data.isWorking ? 'Present' : data.monthOfLeaving + '-' + data.yearOfLeaving }
                                </Text>
                            </Body>
                        </CardItem>
                    ) : <CardItem><Body><Text style={ textStyles.bodyText }>No Data</Text></Body></CardItem> }
                </Card>
            </View>
        </ScrollView >
    );
};

const styles = StyleSheet.create({
    PersonalDetailContainer: {
        marginHorizontal: 50,
        marginVertical: 5,
    },

    EditProfileDetailContainer: {
        marginHorizontal: 20,
        marginVertical: 10,
    },

    errors: {
        fontSize: 12,
        color: 'red',
        margin: 5,
        marginLeft: 16,
    },

    buttonContainer: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },

    EditIcon: {
        alignItems: 'flex-end',
    },

    DetailContainer: {
        justifyContent: 'space-evenly'
    },

    textStyle: {
        color: colors.primary
    },

    DName: {
        fontSize: 14,
        color: '#ccc',
    },

    Name: {
        fontSize: 18,
        color: colors.primary,
        textAlign: 'center',
        marginTop: 10,
    },

    DText: {
        fontSize: 12,
    },

    Dtext: {
        fontSize: 14,
        color: colors.primary,
        textAlign: 'right',
    },

    CardContainer: {
        padding: 10,
    },

    SocialLinksContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: 10
    }
});

export default UserProfileDetail;