export default {
    base: '#ffffff',
    primary: '#295c88',
    secoundry: '#ff6f00',
    accent: '#EDCDF4',
    unRead: '#e6f7ff',
    instagram: '#E1306C',
    facebook: '#3c5a99',
    twitter: '#55adee',
    linkdein: '#2867b2'
}