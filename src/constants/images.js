export default {
    LOGO_SGSITS: 'http://staging.sgsitsalumni.in/static/media/logo.db1e0060.png',
    SGSITS_LOGO: require('../../assets/SGSITSLogo.png'), //In Use
    DEFAULT_PROFILE: 'https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png', //In Use
    DEFAULT_COVER: 'http://staging.sgsitsalumni.in/static/media/default-cover.e2ab1d1b.png', //In Use
    COVER: 'https://st2.depositphotos.com/3503231/7703/v/950/depositphotos_77039327-stock-illustration-blue-white-polygonal-mosaic-background.jpg',
    DEFAULT_JOBLOGO: require('../../assets/DefaultLogo.png'), //In Use
    NO_DATA: require('../../assets/NoData.png') //In Use
}