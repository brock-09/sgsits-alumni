import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  bodyText: {
    fontFamily: 'Roboto_light',
    fontSize: 14,
  },

  regularText: {
    fontFamily: 'Roboto_regular',
    fontSize: 18,
  },

  mediumText: {
    fontFamily: 'Roboto_medium',
    fontSize: 20,
  },

  titleText: {
    fontFamily: 'Roboto_bold',
    fontSize: 22,
  },
});