import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Platform, View, Alert } from 'react-native';
import { Icon, Badge, Text } from 'native-base';
import { useSelector, useDispatch } from 'react-redux';
import { axiosall } from '../util/axiosall';
import { deleteLogin } from '../redux/actions/AuthActions';
import { deleteAllEducation } from '../redux/actions/RegEducationalActions';
import { deleteAllProfessional } from '../redux/actions/RegProfessionalActions';
import { deletePersonal } from '../redux/actions/RegPersonalActions';
import Home from '../screens/Home';
import Network from '../screens/Network';
import Messages from '../screens/Messages';
import Jobs from '../screens/Jobs';
import Profile from '../screens/Profile';
import UserProfile from '../screens/UserProfile';
import NotificationScreen from '../screens/NotificationScreen';
import Connections from '../components/Messages/Connections';
import Chat from '../components/Messages/Chat';
import colors from '../constants/colors';
import textStyles from '../constants/textStyles';
import JobPost from '../components/Home/JobPost';
import JobDetail from '../components/Home/JobDetail';
import ReadJob from '../components/Home/ReadJob';
import api from '../constants/api';

//Function to show UnReadCounts on notifications Icon
const NotificationFun = () => {
    const navigation = useNavigation();
    const { account } = useSelector((state) => state.AuthReducer);
    const [ NotificationCount, setNotificationCount ] = useState();

    const configuration = {
        headers: {
            Authorization: "Bearer " + account[ 0 ].name.token
        },
        params: { accountId: account[ 0 ].name.accountId }
    }

    axiosall('get', api.GET_UNREAD_COUNT, configuration)
        ?.then((response) => {
            setNotificationCount(response.data.notifications)
        })
        .catch((error) => {
            console.log(error)
        })

    return (
        <View style={ { flexDirection: 'row' } }>
            <Icon
                name="md-notifications"
                type="Ionicons"
                style={ { color: colors.base, marginRight: 20 } }
                onPress={ () => navigation.navigate('Notification') } />
            { NotificationCount > 0
                ? <Badge style={ {
                    scaleX: 0.6, scaleY: 0.6, position: 'absolute', bottom: 8, left: 2
                } }>
                    <Text>{ NotificationCount }</Text>
                </Badge>
                : null }
        </View>
    );
}

//Messages Icon with Count Badge
const MessagesIcon = () => {
    const { account } = useSelector((state) => state.AuthReducer);
    const [ MessagesCount, setMessagesCount ] = useState();

    const configuration = {
        headers: {
            Authorization: "Bearer " + account[ 0 ].name.token
        },
        params: { accountId: account[ 0 ].name.accountId }
    }

    axiosall('get', api.GET_UNREAD_COUNT, configuration)
        ?.then((response) => {
            setMessagesCount(response.data.messages)
        })
        .catch((error) => {
            console.log('App MessagesIcon', error)
        })

    return (
        <View style={ { flexDirection: 'row' } }>
            <Icon
                name='envelope'
                type='FontAwesome'
                style={ { color: colors.primary } } />
            { MessagesCount > 0
                ? <Badge style={ {
                    scaleX: 0.6, scaleY: 0.6, position: 'absolute', bottom: 8, left: 15
                } }>
                    <Text>{ MessagesCount }</Text>
                </Badge>
                : null }
        </View>
    );
}

//Function for Logout
const LogoutFun = () => {
    const dispatch = useDispatch();
    const { account } = useSelector((state) => state.AuthReducer);
    const key = account[ 0 ].key;
    return (
        <Icon
            name="logout"
            type="MaterialCommunityIcons"
            style={ { color: colors.base, marginRight: 10 } }
            onPress={ () => Alert.alert('Logout', 'Do you confirm to Logout?', [
                {
                    text: 'Yes', onPress: () => {
                        dispatch(deletePersonal());
                        dispatch(deleteAllProfessional());
                        dispatch(deleteAllEducation());
                        dispatch(deleteLogin(key));
                    }
                },
                { text: 'Cancel', style: 'cancel' },
            ]) } />
    );
}

const HomeStack = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText,
                headerRight: () => (
                    <View style={ { flexDirection: 'row', marginRight: 5 } }>
                        <NotificationFun />
                        <LogoutFun />
                    </View>)
            } }>
            <Stack.Screen name='Home' component={ Home } options={ { title: 'Home' } } />
            <Stack.Screen name='JobPost' component={ JobPost } options={ { title: 'Post Job' } } />
            <Stack.Screen name='JobDetail' component={ JobDetail } options={ { title: 'Post Job' } } />
            <Stack.Screen name='ReadJob' component={ ReadJob } options={ { title: 'Job' } } />
            <Stack.Screen name='UserProfile' component={ UserProfile } options={ { title: 'Profile' } } />
            <Stack.Screen name='Notification' component={ NotificationScreen } options={ { title: 'Notification' } } />
        </Stack.Navigator>
    );
};

const NetworkStack = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText,
                headerRight: () => (
                    <View style={ { flexDirection: 'row', marginRight: 5 } }>
                        <NotificationFun />
                        <LogoutFun />
                    </View>)
            } }>
            <Stack.Screen name='Network' component={ Network } options={ { title: 'Network' } } />
            <Stack.Screen name='UserProfile' component={ UserProfile } options={ { title: 'Profile' } } />
        </Stack.Navigator>
    );
};

const MessagesStack = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText
            } } >
            <Stack.Screen name='Messages' component={ Messages } options={ { title: 'Messages' } } />
            <Stack.Screen name='Connections' component={ Connections } options={ { title: 'Connections' } } />
            <Stack.Screen name='Chat' component={ Chat } />
        </Stack.Navigator>
    );
};

const JobsStack = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText,
                headerRight: () => (
                    <View style={ { flexDirection: 'row', marginRight: 5 } }>
                        <NotificationFun />
                        <LogoutFun />
                    </View>)
            } } >
            <Stack.Screen name='Jobs' component={ Jobs } options={ { title: 'Jobs' } } />
        </Stack.Navigator>
    );
};

const ProfileStack = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText,
                headerRight: () => (
                    <View style={ { flexDirection: 'row', marginRight: 5 } }>
                        <NotificationFun />
                        <LogoutFun />
                    </View>)
            } }>
            <Stack.Screen name='Profile' component={ Profile } options={ { title: 'Profile', headerShown: false } } />
            <Stack.Screen name='UserProfile' component={ UserProfile } options={ { title: 'Profile' } } />
        </Stack.Navigator>
    );
};

const AppNavigation = props => {
    const Tab = createBottomTabNavigator();
    return (
        <Tab.Navigator tabBarOptions={ {
            activeTintColor: colors.primary,
            adaptive: true,
        } } >
            <Tab.Screen name='Home' component={ HomeStack } options={ { tabBarIcon: (tabInfo) => { return <Icon name='home' type='FontAwesome' style={ { color: colors.primary } } /> } } } />
            <Tab.Screen name='Network' component={ NetworkStack } options={ { tabBarIcon: (tabInfo) => { return <Icon name='users' type='FontAwesome' style={ { color: colors.primary } } /> } } } />
            <Tab.Screen name='Messages' component={ MessagesStack } options={ { tabBarIcon: (tabInfo) => { return <MessagesIcon /> } } } />
            <Tab.Screen name='Jobs' component={ JobsStack } options={ { tabBarIcon: (tabInfo) => { return <Icon name='suitcase' type='FontAwesome' style={ { color: colors.primary } } /> } } } />
            <Tab.Screen name='Profile' component={ ProfileStack } options={ { tabBarIcon: (tabInfo) => { return <Icon name='user' type='FontAwesome' style={ { color: colors.primary } } /> } } } />
        </Tab.Navigator>
    );
};

export default AppNavigation;