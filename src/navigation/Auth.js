import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Platform } from 'react-native';
import SignIn from '../screens/SignIn';
import Signup from '../components/Signup/Signup';
import VerifyEmail from '../components/Signup/VerifyEmail';
import colors from '../constants/colors';
import textStyles from '../constants/textStyles';

const Auth = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText,
            } }>
            <Stack.Screen name='SignIn' component={ SignIn } options={ { headerShown: false } } />
            <Stack.Screen name='SignUp' component={ Signup } options={ { title: 'SignUp' } } />
            <Stack.Screen name='VerifyEmail' component={ VerifyEmail } options={ { title: 'Email Verification', headerLeft: null } } />
        </Stack.Navigator>
    );
};

export default Auth;