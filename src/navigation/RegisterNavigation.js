import React from 'react';
import { Platform, Alert } from 'react-native';
import { Icon } from 'native-base';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector, useDispatch } from 'react-redux';
import PersonalDetails from '../components/Register/PersonalDetails';
import EducationalDetails from '../components/Register/EducationalDetails';
import EducationForm from '../components/Register/EducationForm';
import ProfessionalDetails from '../components/Register/ProfessionalDetails';
import { deleteLogin } from '../redux/actions/AuthActions';
import { deleteAllEducation } from '../redux/actions/RegEducationalActions';
import { deleteAllProfessional } from '../redux/actions/RegProfessionalActions';
import { deletePersonal } from '../redux/actions/RegPersonalActions';
import colors from '../constants/colors';
import textStyles from '../constants/textStyles';


const LogoutFun = () => {
    const dispatch = useDispatch();
    const { account } = useSelector((state) => state.AuthReducer);
    const key = account[ 0 ].key;
    return (
        <Icon
            name="logout"
            type="MaterialCommunityIcons"
            style={ { color: colors.base, marginRight: 10 } }
            onPress={ () => Alert.alert('Logout', 'Do you confirm to Logout?', [
                {
                    text: 'Yes', onPress: () => {
                        dispatch(deletePersonal());
                        dispatch(deleteAllProfessional());
                        dispatch(deleteAllEducation());
                        dispatch(deleteLogin(key));
                    }
                },
                { text: 'Cancel', style: 'cancel' },
            ]) } />
    );
}

const RegisterNavigation = props => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator
            screenOptions={ {
                headerStyle: { backgroundColor: Platform.OS === 'android' ? colors.primary : colors.base },
                headerTintColor: Platform.OS === 'android' ? colors.base : colors.primary,
                headerTitleStyle: textStyles.titleText,
                headerTitleAlign: 'center',
                headerRight: () => <LogoutFun />
            } }>
            <Stack.Screen name='PersonalDetails' component={ PersonalDetails } options={ { title: 'Personal' } } />
            <Stack.Screen name='EducationalDetails' component={ EducationalDetails } options={ { title: 'Education' } } />
            <Stack.Screen name='EducationForm' component={ EducationForm } options={ { title: 'Education' } } />
            <Stack.Screen name='ProfessionalDetails' component={ ProfessionalDetails } options={ { title: 'Professional' } } />
        </Stack.Navigator>

    );
};

export default RegisterNavigation;