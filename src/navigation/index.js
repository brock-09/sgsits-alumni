import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import Auth from './Auth';
import AppNavigation from './AppNavigation';
import RegisterNavigation from './RegisterNavigation';

export default () => {
    const { account } = useSelector(state => state.AuthReducer);
    return (
        <NavigationContainer>
            { account[ 0 ] !== undefined
                ? account[ 0 ].name.accountId && account[ 0 ].name.token && account[ 0 ].name.isUserRegistered === true
                    ? <AppNavigation />
                    : account[ 0 ].name.accountId && account[ 0 ].name.token && account[ 0 ].name.isUserRegistered === false
                        ? <RegisterNavigation /> : null
                : <Auth /> }
        </NavigationContainer>
    );
};


