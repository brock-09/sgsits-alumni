import { ADD_ACCOUNT, LOGOUT } from './types';

export const addAuth = (Auth) => (
    {
        type: ADD_ACCOUNT,
        data: Auth
    }
);

export const deleteLogin = (key) => (
    {
        type: LOGOUT,
        key: key
    }
);