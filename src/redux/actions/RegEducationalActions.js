import { ADD_EDUCATIONAL, DELETE_EDUCATIONAL, DELETE_ALL_EDUCATIONAL } from './types';

export const addEducation = (data) => (
    {
        type: ADD_EDUCATIONAL,
        data: data
    }
);

export const deleteEducation = (key) => (
    {
        type: DELETE_EDUCATIONAL,
        key: key
    }
);

export const deleteAllEducation = () => (
    {
        type: DELETE_ALL_EDUCATIONAL
    }
);