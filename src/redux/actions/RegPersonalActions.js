import { ADD_PERSONAL, DELETE_PERSONAL } from './types';

export const addPersonal = (data) => (
    {
        type: ADD_PERSONAL,
        data: data
    }
);

export const deletePersonal = () => (
    {
        type: DELETE_PERSONAL
    }
);