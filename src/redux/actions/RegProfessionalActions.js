import { ADD_PROFESSIONAL, DELETE_PROFESSIONAL, DELETE_ALL_PROFESSIONAL } from './types';

export const addProfessional = (data) => (
    {
        type: ADD_PROFESSIONAL,
        data: data
    }
);

export const deleteProfessional = (key) => (
    {
        type: DELETE_PROFESSIONAL,
        key: key
    }
);

export const deleteAllProfessional = () => (
    {
        type: DELETE_ALL_PROFESSIONAL
    }
);