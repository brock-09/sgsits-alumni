export const ADD_ACCOUNT = 'ADD_ACCOUNT';
export const LOGOUT = 'LOGOUT';
//For Registration: Personal
export const ADD_PERSONAL = 'ADD_PERSONAL';
export const DELETE_PERSONAL = 'DELETE_PERSONAL';
//For Registration: Educational
export const ADD_EDUCATIONAL = 'ADD_EDUCATIONAL';
export const DELETE_EDUCATIONAL = 'DELETE_EDUCATIONAL';
export const DELETE_ALL_EDUCATIONAL = 'DELETE_ALL_EDUCATIONAL';
//For Registration: Professional
export const ADD_PROFESSIONAL = 'ADD_PROFESSIONAL';
export const DELETE_PROFESSIONAL = 'DELETE_PROFESSIONAL';
export const DELETE_ALL_PROFESSIONAL = 'DELETE_ALL_PROFESSIONAL';