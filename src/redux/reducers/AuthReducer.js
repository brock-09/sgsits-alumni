import { ADD_ACCOUNT, LOGOUT } from '../actions/types';

const initialState = {
    account: []
}

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ACCOUNT:
            return {
                ...state,
                account: state.account.concat({
                    key: Math.random(),
                    name: action.data
                })
            };
        case LOGOUT:
            return {
                account: [],
            };
        default:
            return state;
    }
}

export default AuthReducer;
