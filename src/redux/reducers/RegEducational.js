import { ADD_EDUCATIONAL, DELETE_EDUCATIONAL, DELETE_ALL_EDUCATIONAL } from '../actions/types';

const initialState = {
    educational: []
}

const RegEducational = (state = initialState, action) => {
    switch (action.type) {
        case ADD_EDUCATIONAL:
            return {
                ...state,
                educational: state.educational.concat(action.data)
            };
        case DELETE_EDUCATIONAL:
            return {
                ...state,
                educational: state.educational.filter((item) =>
                    item.courseId !== action.key)
            };
        case DELETE_ALL_EDUCATIONAL:
            return {
                educational: [],
            };
        default:
            return state;
    }
}

export default RegEducational;
