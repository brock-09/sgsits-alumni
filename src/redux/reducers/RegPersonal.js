import { ADD_PERSONAL, DELETE_PERSONAL } from '../actions/types';

const initialState = {
    personal: {}
}

const RegPersonal = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PERSONAL:
            return {
                personal: { data: action.data }
            };
        case DELETE_PERSONAL:
            return {
                personal: {},
            };
        default:
            return state;
    }
}

export default RegPersonal;
