import { ADD_PROFESSIONAL, DELETE_PROFESSIONAL, DELETE_ALL_PROFESSIONAL } from '../actions/types';

const initialState = {
    professional: []
}

const RegProfessional = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PROFESSIONAL:
            return {
                ...state,
                professional: state.professional.concat(action.data)
            };
        case DELETE_PROFESSIONAL:
            return {
                ...state,
                professional: state.professional.filter((item) =>
                    item.id !== action.key)
            };
        case DELETE_ALL_PROFESSIONAL:
            return {
                professional: [],
            };
        default:
            return state;
    }
}

export default RegProfessional;
