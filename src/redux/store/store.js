import { createStore, combineReducers, applyMiddleware } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer, persistStore } from 'redux-persist';
import AuthReducer from '../reducers/AuthReducer';
import RegPersonal from '../reducers/RegPersonal';
import RegEducational from '../reducers/RegEducational';
import RegProfessional from '../reducers/RegProfessional';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
}

const AuthPersistConfig = {
    key: 'Auth',
    storage: AsyncStorage,
}

const RegisterPersistConfig = {
    key: 'Register',
    storage: AsyncStorage,
}

const rootReducer = combineReducers({
    AuthReducer: persistReducer(AuthPersistConfig, AuthReducer),
    RegPersonal: persistReducer(RegisterPersistConfig, RegPersonal),
    RegEducational: persistReducer(RegisterPersistConfig, RegEducational),
    RegProfessional: persistReducer(RegisterPersistConfig, RegProfessional)
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(persistedReducer, applyMiddleware())
export const persistor = persistStore(store)

