import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { View, Text, Card, CardItem, ListItem, Thumbnail, Body, Left, Right, Icon } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../util/axiosall';
import FeedCardItem from '../components/Home/FeedCardItem';
import LoadingScreen from '../components/LoadingScreen';
import colors from '../constants/colors';
import textStyles from '../constants/textStyles';
import api from '../constants/api';
import images from '../constants/images';
import NoData from '../components/NoData';

const Home = props => {
    const [ loading, setLoading ] = useState(false);
    const [ UserDetailData, setUserDetailData ] = useState();
    const [ TimelineData, setTimelineData ] = useState();

    useEffect(() => {
        setLoading(true);
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: {
                accountId: accountDetails.accountId,
                id: accountDetails.accountId
            }
        };

        axiosall('get', api.GET_PROFILE_BY_ID, configuration)
            ?.then((response) => {
                const result = response.data;
                setUserDetailData(result.profile);
                setLoading(false);
            })
            .catch(({ ...error }) => {
                console.log(error);
            });

        axiosall('get', api.GET_TIMELINE_INFO, configuration)
            ?.then((response) => {
                setTimelineData(response.data)
            })
            .catch(({ ...error }) => {
                console.log(error);
            });
    }, []);

    if (loading || UserDetailData === undefined || TimelineData === undefined) {
        return <LoadingScreen />;
    }

    const TimelineCards = result => {
        return <FeedCardItem data={ result.item } />
    }

    return (
        <View style={ styles.container }>
            <ListItem thumbnail>
                <Left>
                    <Thumbnail circular source={ {
                        uri: UserDetailData.profilePic != null
                            ? UserDetailData.profilePic
                            : images.DEFAULT_PROFILE
                    } } />
                </Left>
                <Body style={ styles.NoBorder }>
                    <Card style={ styles.cardStyle }>
                        <CardItem button onPress={ () => props.navigation.navigate('JobPost') }>
                            <Icon name='ios-add' type='Ionicons' style={ styles.JobPost } />
                            <Text style={ { ...textStyles.bodyText, ...styles.JobPost } }>
                                Create Job Post
                            </Text>
                        </CardItem>
                    </Card>
                </Body>
                <Right style={ styles.NoBorder }></Right>
            </ListItem>

            <CardItem bordered style={ styles.Border }>
                <Text style={ { ...textStyles.regularText, ...styles.JobPost } }>Activity Feeds</Text>
            </CardItem>

            { TimelineData[ 0 ] !== undefined
                ? <FlatList
                    keyExtractor={ (item, index) => index }
                    data={ TimelineData }
                    renderItem={ TimelineCards } />
                : <NoData message={ 'Start Activities to see them here...' } /> }
        </View >
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.base
    },

    cardStyle: {
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    JobPost: {
        marginHorizontal: 3,
        color: colors.primary,
    },

    HomeError: {
        color: colors.primary,
        textAlign: 'center'
    },

    Border: {
        borderTopWidth: 0.5
    },

    NoBorder: {
        borderBottomWidth: 0
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Home);