import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { Container, View, Text } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../util/axiosall';
import SearchHeader from '../components/SearchHeader';
import JobsCard from '../components/Jobs/JobsCard';
import LoadingScreen from '../components/LoadingScreen';
import NoData from '../components/NoData';
import colors from '../constants/colors';
import api from '../constants/api';


const Jobs = props => {

    const [ JobsData, setJobsData ] = useState();
    const [ filterJobsData, setfilterJobsData ] = useState();

    useEffect(() => {
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
        };

        axiosall('get', api.GET_JOBS, configuration)
            .then((response) => {
                setJobsData(response.data);
                setfilterJobsData(response.data);
            })
            .catch((error) => {
                console.log('Jobs Get', error)
            });
    }, [])

    const onSearch = (search) => {
        setfilterJobsData(
            JobsData.filter(data => {
                if (search != undefined && data.companyName != null && data.jobRequiredSkills != null && data.jobLocation != null && data.companyWebsite != null
                    && data.companyEmailAddress != null && data.designation != null && data.companyAddress != null && data.jobShortDescription != null) {
                    return data.companyName.toLowerCase().includes(search) +
                        data.jobRequiredSkills.toLowerCase().includes(search) +
                        data.jobLocation.toLowerCase().includes(search) +
                        data.companyWebsite.toLowerCase().includes(search) +
                        data.companyEmailAddress.toLowerCase().includes(search) +
                        data.designation.toLowerCase().includes(search) +
                        data.companyAddress.toLowerCase().includes(search) +
                        data.jobShortDescription.toLowerCase().includes(search)
                }
            })
        )
    }

    const onClear = () => {
        setfilterJobsData(JobsData);
    }

    if (filterJobsData === undefined) {
        return <LoadingScreen />
    }

    const JobsCardData = (result) => {
        return <JobsCard data={ result.item } />
    }

    return (
        <Container>
            <SearchHeader onSearch={ onSearch } onClear={ onClear } />
            <View style={ styles.Content }>
                { filterJobsData[ 0 ] !== undefined
                    ? <FlatList
                        keyExtractor={ (item, index) => index }
                        data={ filterJobsData }
                        renderItem={ JobsCardData } />
                    : <NoData message={ 'No Job Found' } /> }
            </View>
        </Container >
    );
};

const styles = StyleSheet.create({

    Content: {
        flex: 1,
        margin: 10,
    },

    JobError: {
        color: colors.primary,
        textAlign: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Jobs);