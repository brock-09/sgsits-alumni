import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { View, Text, Fab, Icon, Body, List, ListItem, Left, Right, Thumbnail } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../util/axiosall';
import moment from 'moment';
import LoadingScreen from '../components/LoadingScreen';
import textStyles from '../constants/textStyles';
import colors from '../constants/colors';
import images from '../constants/images';
import api from '../constants/api';
import NoData from '../components/NoData';

const Messages = props => {
    const [ Threads, setThreads ] = useState();

    useEffect(() => {
        //Getting onHoing Threads
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { id: accountDetails.accountId }
        };

        axiosall('get', api.GET_THREADS, configuration)
            .then((response) => {
                setThreads(response.data);
            })
            .catch((error) => {
                console.log('Message Threads', error);
            });

    })

    const ThreadsCards = itemData => {
        return (
            <List>
                <ListItem thumbnail onPress={ () => props.navigation.navigate('Chat', itemData.item) }>
                    <Left>
                        <Thumbnail circular source={ {
                            uri: itemData.item.profilePic !== null
                                ? itemData.item.profilePic
                                : images.DEFAULT_PROFILE
                        } } />
                    </Left>
                    <Body>
                        <Text style={ textStyles.regularText }>
                            { itemData.item.name }
                        </Text>

                        { itemData.item.latestJobTitle !== null
                            ? <Text note style={ textStyles.bodyText }>{ itemData.item.lastMessage }</Text>
                            : null }
                    </Body>
                    <Right>
                        <Text note style={ { ...textStyles.bodyText, fontSize: 12 } }>
                            { moment(itemData.item.createdAt, 'YYYY-MM-DD').format('DD/MM/YYYY') }
                        </Text>
                    </Right>
                </ListItem>
            </List>
        );
    }

    if (Threads === undefined) {
        return <LoadingScreen />;
    }

    return (
        <View style={ styles.container }>
            { Threads[ 0 ] !== undefined
                ? < FlatList
                    keyExtractor={ (item, index) => item.id }
                    data={ Threads }
                    renderItem={ ThreadsCards } />
                : <NoData message={ 'Start Messaging, to see them here...' } /> }

            <Fab
                style={ { backgroundColor: colors.primary } }
                position="bottomRight"
                onPress={ () => props.navigation.navigate('Connections') }>
                <Icon name="android-messages" type='MaterialCommunityIcons' />
            </Fab>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.base,
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Messages);