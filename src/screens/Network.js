import React, { useState, useEffect } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { Container, Text } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../util/axiosall';
import LoadingScreen from '../components/LoadingScreen';
import NetworkCardItem from '../components/Network/NetworkCardItem';
import SearchHeader from '../components/SearchHeader';
import textStyles from '../constants/textStyles';
import colors from '../constants/colors';
import api from '../constants/api';
import NoData from '../components/NoData';

const Network = props => {

    //UseStates to saveListOfUsers and implement search
    const [ PeoplesData, setPeoplesData ] = useState();
    const [ filterPeoplesData, setFilterPeoplesData ] = useState();

    useEffect(() => {
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: accountDetails.accountId }
        };

        axiosall('get', api.GET_PEOPLES, configuration)
            ?.then((response) => {
                //Saving response in UseStates
                setPeoplesData(response.data)
                setFilterPeoplesData(response.data)
            })
            .catch((error) => {
                console.log('Error', error);
            });
    }, [])

    if (filterPeoplesData === undefined) {
        return <LoadingScreen />
    }

    //OnSeach method for filtering Users
    const onSearch = (search) => {
        setFilterPeoplesData(
            PeoplesData.filter(data => {
                //Conditions to avoid null entries in search created during Testing and is a safe point
                if (search != undefined && data.firstName != null && data.lastName != null && data.batch != null && data.latestJobTitle != null) {
                    let name = data.firstName + " " + data.lastName;
                    return name.toLowerCase().includes(search.toLowerCase()) +
                        data.firstName.toLowerCase().includes(search) +
                        data.lastName.toLowerCase().includes(search) +
                        data.latestJobTitle.toLowerCase().includes(search) +
                        data.batch.toLowerCase().includes(search)
                }
            })
        )
    }

    //OnClear method toh reinitialize FilterData
    const onClear = () => {
        setFilterPeoplesData(PeoplesData);
    }

    const UsersCardItem = result => {
        return <NetworkCardItem data={ result.item } />
    }

    return (
        <Container>
            <SearchHeader onSearch={ onSearch } onClear={ onClear } />

            <View style={ styles.Content }>
                { filterPeoplesData[ 0 ] != undefined
                    ? <FlatList
                        keyExtractor={ (item, index) => index }
                        data={ filterPeoplesData }
                        renderItem={ UsersCardItem }
                        numColumns={ 2 } />
                    : <NoData message={ 'No Profile Found' } /> }
            </View>
        </Container >
    )
}
const styles = StyleSheet.create({
    Content: {
        flex: 1,
        margin: 20,
    },

    Text: {
        color: colors.primary,
        textAlign: 'center',
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Network);