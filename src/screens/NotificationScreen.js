import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { View, Text } from 'native-base';
import { connect } from 'react-redux';
import axios from 'axios';
import NotificationCard from '../components/Notifications/NotificationCard';
import LoadingScreen from '../components/LoadingScreen';
import textStyles from '../constants/textStyles';
import colors from '../constants/colors';
import { axiosall } from '../util/axiosall';
import api from '../constants/api';

const NotificationScreen = props => {

    const [ ReadNotifications, setReadNotifications ] = useState();
    const [ UnreadNotifications, setUnreadNotifications ] = useState();

    useEffect(() => {
        //Getting Read and UnreadNotifications
        const accountDetails = props.Auths[ 0 ].name;

        let configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token,
            },
            params: { accountId: accountDetails.accountId }
        }

        axiosall('get', api.GET_UNREAD_NOTIFICATION, configuration)
            ?.then((response) => {
                setUnreadNotifications(response.data.reverse());
            })
            .catch((error) => {
                console.log('UnRead Notifications Error', error);
            })

        axiosall('get', api.GET_READ_NOTIFICATION, configuration)
            ?.then((response) => {
                setReadNotifications(response.data.reverse());
            })
            .catch((error) => {
                console.log('Read Notifications Error', error);
            })

    }, []);

    //Function to Respond to Recived Requests
    const respondToConnectionRequest = (connectionData, status) => {
        const accountDetails = props.Auths[ 0 ].name;
        const data = { id: connectionData.connectionId, status: status };
        let params = {
            url: api.BASE + api.APPROVE_CONNECTION,
            configuration: {
                headers: {
                    Authorization: "Bearer " + accountDetails.token
                },
            }
        }

        axios.post(params.url, data, params.configuration)
            .then((response) => {
                return props.navigation.push('Notification')
            })
            .catch((error) => {
                console.log('Error', error);
            })
    };

    if (ReadNotifications === undefined || UnreadNotifications === undefined) {
        return <LoadingScreen />
    }

    const NotificationsCard = itemData => {
        return <NotificationCard data={ itemData.item } onResponse={ respondToConnectionRequest } />
    }


    return (
        <View style={ styles.Container }>
            { UnreadNotifications[ 0 ] === undefined && ReadNotifications[ 0 ] === undefined
                ? <Text style={ { ...textStyles.regularText, alignSelf: 'center', margin: 30 } }>No Notifications</Text>
                : <FlatList
                    keyExtractor={ (item, index) => item.id }
                    data={ UnreadNotifications }
                    renderItem={ NotificationsCard } />
                && <FlatList
                    keyExtractor={ (item, index) => item.id }
                    data={ ReadNotifications }
                    renderItem={ NotificationsCard } /> }
        </View>
    );
};

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: colors.base
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(NotificationScreen);