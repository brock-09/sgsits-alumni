import React, { useState, useEffect } from 'react';
import { ImageBackground, Image, View, StyleSheet, Dimensions, Modal } from 'react-native';
import { Tabs, Tab, Icon, Text, Card, CardItem } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../util/axiosall';
import * as ImagePicker from 'expo-image-picker';
import axios from 'axios';
import colors from '../constants/colors';
import ProfileDetail from '../components/Profile/ProfileDetail';
import ProfileConnection from '../components/Profile/ProfileConnection';
import ProfileSettings from '../components/Profile/ProfileSettings';
import LoadingScreen from '../components/LoadingScreen';
import api from '../constants/api';
import images from '../constants/images';

const Profile = props => {
    //Main myProfileScreen Component
    const [ loading, setLoading ] = useState(false);
    const [ data, setData ] = useState();
    const [ UserDetailData, setUserDetailData ] = useState();
    const [ EditProfile, setEditProfile ] = useState(false);

    useEffect(() => {
        setLoading(true);
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: accountDetails.accountId }
        };

        axiosall('get', api.GET_PROFILE_BY_ID, configuration)
            ?.then((response) => {
                const result = response.data;
                setData(result);
                setUserDetailData(result.profile);
                setLoading(false);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);

    const editCover = async (data) => {
        await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Image,
            allowsEditing: true,
            base64: true,
            aspect: data === 'profilePic' ? [ 3, 3 ] : undefined,
            quality: 1
        })
            .then((response) => {
                if (!response.cancelled) {
                    setLoading(true);
                    const accountDetails = props.Auths[ 0 ].name;
                    let params = {
                        url: api.BASE + api.UPDATE_PROFILE,
                        configuration: {
                            headers: { 'Authorization': "Bearer " + accountDetails.token }
                        },
                        data: {
                            accountId: accountDetails.accountId,
                            personal: { [ data ]: `data:image/png;base64,${ response.base64 }` }
                        }
                    }

                    axios.put(params.url, params.data, params.configuration)
                        .then((response) => {
                            setUserDetailData(response.data.data[ 0 ]);
                            setLoading(false);
                        })
                        .catch((error) => {
                            console.log(error)
                            setLoading(false);
                        })
                }
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    };

    const clickProfile = async (data) => {
        await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Image,
            allowsEditing: true,
            base64: true,
            aspect: data === 'profilePic' ? [ 3, 3 ] : undefined,
            quality: 1
        })
            .then((response) => {
                if (!response.cancelled) {
                    setLoading(true);
                    const accountDetails = props.Auths[ 0 ].name;
                    let params = {
                        url: api.BASE + api.UPDATE_PROFILE,
                        configuration: {
                            headers: { 'Authorization': "Bearer " + accountDetails.token }
                        },
                        data: {
                            accountId: accountDetails.accountId,
                            personal: { [ data ]: `data:image/png;base64,${ response.base64 }` }
                        }
                    }

                    axios.put(params.url, params.data, params.configuration)
                        .then((response) => {
                            setUserDetailData(response.data.data[ 0 ]);
                            setLoading(false);
                        })
                        .catch((error) => {
                            console.log(error)
                            setLoading(false);
                        })
                }
            })
            .catch((error) => {
                console.log(error);
                setLoading(false);
            })
    };


    //LoadingScreenComponent
    if (loading || UserDetailData === undefined) {
        return <LoadingScreen />;
    }

    return (
        <View style={ styles.screen }>
            <View style={ styles.bgimageContainer }>
                <ImageBackground
                    source={ {
                        uri: UserDetailData.bannerPic !== null
                            ? UserDetailData.bannerPic
                            : images.DEFAULT_COVER
                    } }
                    style={ styles.bgImage } >
                    <View style={ styles.imageContainer }>
                        <Image
                            source={ {
                                uri: UserDetailData.profilePic !== null
                                    ? UserDetailData.profilePic
                                    : images.DEFAULT_PROFILE
                            } }
                            style={ styles.bgImage } />
                        <Icon
                            name='pencil'
                            type='FontAwesome'
                            style={ { ...styles.EditIcon, ...styles.ProfileEditIcon } }
                            onPress={ () => setEditProfile(true) } />
                    </View>
                    <Icon
                        name='pencil'
                        type='FontAwesome'
                        style={ styles.EditIcon }
                        onPress={ () => editCover('bannerPic') } />
                </ImageBackground>
            </View>
            <Tabs tabBarUnderlineStyle={ { backgroundColor: colors.primary } }>
                <Tab heading="PROFILE" tabStyle={ styles.tabs } textStyle={ styles.textTabs } activeTabStyle={ styles.activeTabs } activeTextStyle={ styles.activeTabs }>
                    <ProfileDetail data={ data } />
                </Tab>
                <Tab heading="CONNECTIONS" tabStyle={ styles.tabs } textStyle={ styles.textTabs } activeTabStyle={ styles.activeTabs } activeTextStyle={ styles.activeTabs }>
                    <ProfileConnection />
                </Tab>
                <Tab heading="SETTINGS" tabStyle={ styles.tabs } textStyle={ styles.textTabs } activeTabStyle={ styles.activeTabs } activeTextStyle={ styles.activeTabs }>
                    <ProfileSettings />
                </Tab>
            </Tabs>
            {/* Modal To EditProfile Photo */ }
            <Modal
                animationType="slide"
                transparent={ true }
                visible={ EditProfile }
                onRequestClose={ () => { setEditProfile(!EditProfile) } }>
                <View style={ styles.modalStyle }>
                    <Card style={ styles.modalCard }>
                        <CardItem>
                            <Icon name='camera' type='MaterialCommunityIcons' style={ styles.textTabs } />
                            <Text onPress={ () => {
                                setEditProfile(!EditProfile);
                                clickProfile('profilePic')
                            } }>Click a Picture</Text>
                        </CardItem>
                        <CardItem>
                            <Icon name='image-plus' type='MaterialCommunityIcons' style={ styles.textTabs } />
                            <Text onPress={ () => {
                                setEditProfile(!EditProfile);
                                editCover('profilePic')
                            } }>Select a Picture</Text>
                        </CardItem>
                        <CardItem>
                            <Icon name='cancel' type='MaterialIcons' style={ styles.textTabs } />
                            <Text onPress={ () => {
                                setEditProfile(!EditProfile);
                            } }>Cancel</Text>
                        </CardItem>
                    </Card>
                </View>
            </Modal>
        </View >
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.base,
    },

    bgimageContainer: {
        height: '35%',
    },

    imageContainer: {
        height: 120,
        width: 120,
        borderRadius: Math.floor(Dimensions.get('window').width * 0.7 / 2),
        borderWidth: 2,
        borderColor: colors.base,
        overflow: 'hidden',
    },

    bgImage: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    tabs: {
        backgroundColor: colors.base,
    },

    textTabs: {
        color: colors.primary,
    },

    activeTabs: {
        color: colors.primary,
        backgroundColor: colors.base,
    },

    EditIcon: {
        color: colors.primary,
        backgroundColor: '#F1F1F1',
        fontSize: 18,
        padding: 5,
        right: 5,
        bottom: 5,
        borderRadius: 50,
        position: 'absolute',
        alignSelf: 'flex-end',
    },

    ProfileEditIcon: {
        position: 'absolute',
        alignSelf: 'center',
        right: 'auto',
        bottom: 0
    },

    modalStyle: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
    },

    modalCard: {
        padding: 20,
        borderRadius: 20,
        width: '100%',
    }
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(Profile);