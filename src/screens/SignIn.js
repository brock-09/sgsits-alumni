import React, { useState } from 'react';
import { StyleSheet, View, Image, Dimensions, Modal } from 'react-native';
import { Container, Content, Form, Text, Toast, Card, CardItem, Item, Right, Icon } from 'native-base';
import { TextInput } from 'react-native-paper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { axiosall } from '../util/axiosall';
import { connect } from 'react-redux';
import { addAuth } from '../redux/actions/AuthActions';
import AppButton from '../components/AppButton';
import AppButtonSec from '../components/AppButtonSec';
import textStyles from '../constants/textStyles';
import colors from '../constants/colors';
import api from '../constants/api';
import images from '../constants/images';

const SignIn = props => {

    const [ ShowForgot, setShowForgot ] = useState(false);

    const initialValues = {
        email: '',
        password: '',
    }

    const ToastMessage = message => {
        Toast.show({
            text: message,
            buttonText: 'Okay',
            buttonTextStyle: { color: '#ccc' },
            textStyle: { ...textStyles.bodyText, color: colors.base },
            style: { margin: 10, borderRadius: 5, backgroundColor: colors.primary },
            duration: 3000
        })
    }

    const onSubmit = values => {
        if (!formik.errors.email && !formik.errors.password) {
            axiosall('post', api.LOGIN, values)
                ?.then((response) => {
                    const result = response.data;
                    props.addAuth(result);
                })
                .catch(({ ...error }) => {
                    if (error && error.response.data.msg) {
                        ToastMessage(error.response.data.msg)
                    } else {
                        ToastMessage('Invalid Request')
                    }
                })
        }
    }

    const onReset = values => {
        formik.handleReset
        setShowForgot(!ShowForgot);
    }

    const validationSchema = Yup.object({
        email: Yup.string()
            .trim('Whitespaces not allowed')
            .email('Invalid Format')
            .required('Required'),
        password: Yup.string()
            .trim('Whitespaces not allowed')
            .min(6, 'Min. characters 6')
            .required('Required')
    })

    const ForgotPassword = () => {
        formik.handleSubmit
        let values = { email: formik.values.email }
        if (!formik.errors.email) {
            axiosall('post', api.FORGET_PASSWORD, values)
                ?.then((response) => {
                    ToastMessage(response.data.msg)
                })
                .catch(({ ...error }) => {
                    ToastMessage(error.response.data);
                })
        }
    }

    const formik = useFormik({
        initialValues,
        onSubmit,
        onReset,
        validationSchema,
    });

    return (
        <Container>
            <Content contentContainerStyle={ styles.Content }>
                <View style={ styles.imageContainer }>
                    <Image source={ images.SGSITS_LOGO } style={ styles.bgImage } />
                </View>
                <View style={ styles.formContainer }>
                    <Form>
                        <TextInput
                            label='Email'
                            mode='outlined'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary } } }
                            keyboardType='email-address'
                            autoCapitalize='none'
                            returnKeyType='next'
                            autoCompleteType='email'
                            textContentType='emailAddress'
                            onBlur={ formik.handleBlur('email') }
                            onChangeText={ formik.handleChange('email') }
                            value={ formik.values.email }
                            error={ formik.touched.email && formik.errors.email ? true : false } />
                        { formik.touched.email && formik.errors.email ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.email }</Text> : null }

                        <TextInput
                            label='Password'
                            mode='outlined'
                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary } } }
                            secureTextEntry={ true }
                            returnKeyType='done'
                            onBlur={ formik.handleBlur('password') }
                            onChangeText={ formik.handleChange('password') }
                            value={ formik.values.password }
                            error={ formik.touched.password && formik.errors.password ? true : false } />
                        { formik.touched.password && formik.errors.password ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.password }</Text> : null }

                        <Text style={ { ...textStyles.bodyText, ...styles.ForgetPasswordLink } } onPress={ () => setShowForgot(!ShowForgot) }>Forgot Password?</Text>

                        <View style={ styles.buttonContainer }>
                            <AppButton title='Sign In' type='submit' onPress={ formik.handleSubmit }> Sign In </AppButton>
                        </View>
                    </Form>

                    <View style={ styles.SignupLink }>
                        <Text style={ textStyles.bodyText }>Don't have an account?</Text>
                        <View style={ styles.buttonLinkContainer }>
                            <AppButtonSec title='Sign Up' onPress={ () => props.navigation.navigate('SignUp') }>Sign Up</AppButtonSec>
                        </View>
                    </View>

                    {/* Modal for Forgotten Password */ }
                    <Modal
                        animationType="slide"
                        transparent={ true }
                        visible={ ShowForgot }
                        onRequestClose={ () => { setShowForgot(!ShowForgot) } }>
                        <View style={ styles.modalStyle }>
                            <Card style={ { borderRadius: 15, overflow: 'hidden' } }>
                                <CardItem>
                                    <Item last>
                                        <Text style={ { ...textStyles.titleText, color: colors.primary } }>Forgot Password?</Text>
                                    </Item>
                                    <Right>
                                        <Icon name='cross' type='Entypo' style={ { color: colors.primary } } onPress={ () => setShowForgot(!ShowForgot) } />
                                    </Right>
                                </CardItem>
                                <View style={ styles.formContainer }>
                                    <Form>
                                        <TextInput
                                            label='Email'
                                            mode='outlined'
                                            theme={ { colors: { placeholder: colors.primary, primary: colors.primary } } }
                                            keyboardType='email-address'
                                            autoCapitalize='none'
                                            returnKeyType='next'
                                            autoCompleteType='email'
                                            textContentType='emailAddress'
                                            onBlur={ formik.handleBlur('email') }
                                            onChangeText={ formik.handleChange('email') }
                                            value={ formik.values.email }
                                            error={ formik.touched.email && formik.errors.email ? true : false } />
                                        { formik.touched.email && formik.errors.email ? <Text style={ { ...textStyles.bodyText, ...styles.errors } }>{ formik.errors.email }</Text> : null }

                                        <View style={ styles.forgotButtonContainer }>
                                            <AppButton title='Send Link' type='submit' onPress={ () => ForgotPassword() }>Send Link</AppButton>
                                            <AppButtonSec title='Cancel' onPress={ formik.handleReset }>Cancel</AppButtonSec>
                                        </View>
                                    </Form>
                                </View>
                            </Card>
                        </View>
                    </Modal>
                </View>
            </Content>
        </Container >
    );
};

const styles = StyleSheet.create({

    Content: {
        flex: 1,
        justifyContent: 'center',
    },

    imageContainer: {
        borderRadius: Math.floor(Dimensions.get('window').width * 0.7 / 2),
        overflow: 'hidden',
        alignItems: 'center',
    },

    bgImage: {
        height: 80,
        width: 80,
    },

    formContainer: {
        margin: 10,
    },

    buttonContainer: {
        margin: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    buttonLinkContainer: {
        margin: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    errors: {
        fontSize: 12,
        color: 'red',
        marginTop: 5,
        marginLeft: 16,
    },

    SignupLink: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    ForgetPasswordLink: {
        alignSelf: 'flex-end',
        color: colors.primary,
        marginVertical: 5
    },

    modalStyle: {
        flex: 1,
        justifyContent: 'center',
        margin: 5
    },

    forgotButtonContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },

});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addAuth: (Auth) => {
            dispatch(addAuth(Auth));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);