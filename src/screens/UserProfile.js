import React, { useState, useEffect } from 'react';
import { ImageBackground, Image, View, StyleSheet, Dimensions } from 'react-native';
import { Tabs, Tab } from 'native-base';
import { connect } from 'react-redux';
import { axiosall } from '../util/axiosall';
import axios from 'axios';
import LoadingScreen from '../components/LoadingScreen';
import UserProfileDetail from '../components/UserProfile/UserProfileDetail';
import UserProfileConnection from '../components/UserProfile/UserProfileConnection';
import colors from '../constants/colors';
import api from '../constants/api';
import images from '../constants/images';

const UserProfile = props => {
    //Main userProfileScreen Component
    const [ loading, setLoading ] = useState(false);
    const [ data, setData ] = useState();
    const [ UserDetailData, setUserDetailData ] = useState();

    const [ ViewID, setViewID ] = useState();
    const [ ConnectionStatus, setConnectionStatus ] = useState();

    useEffect(() => {
        setLoading(true);
        //Fetching userAccountDetails for token and Account from AuthReducer
        const accountDetails = props.Auths[ 0 ].name;
        const viewId = props.route.params.id;
        setViewID(viewId);

        if (viewId === accountDetails.accountId) {
            return props.navigation.navigate('Profile');
        }

        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: viewId }
        };

        axiosall('get', api.GET_PROFILE_BY_ID, configuration)
            ?.then((response) => {
                const result = response.data;
                setData(result);
                setUserDetailData(result.profile);
                setLoading(false);
            })
            .catch((error) => {
                console.log('Get User Profile', error);
            });

        const Configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            },
            params: { accountId: accountDetails.accountId }
        };

        axiosall('get', api.GET_CONNECTION, Configuration)
            ?.then((response) => {
                response.data.forEach(element => {
                    if (element.accountId === viewId || element.connectionId === viewId) {
                        setConnectionStatus(element.status)
                    }
                });
            })
            .catch((error) => {
                console.log('GET Connection', error);
            });

    }, []);


    const sendConnectionRequest = () => {
        const accountDetails = props.Auths[ 0 ].name;
        const data = {
            accountId: accountDetails.accountId,
            connectionId: ViewID
        }
        const configuration = {
            headers: {
                Authorization: "Bearer " + accountDetails.token
            }
        }
        axios.post(api.BASE + api.ADD_CONNECTION, data, configuration)
            ?.then((response) => {
                setConnectionStatus(response.data.status)
            })
            .catch((error) => {
                console.log('SendConnectionRequest', error);
            });
    }


    if (loading || UserDetailData === undefined) {
        return <LoadingScreen />;
    }

    return (
        <View style={ styles.screen }>
            <View style={ styles.bgimageContainer }>
                <ImageBackground
                    source={ {
                        uri: UserDetailData.bannerPic !== null
                            ? UserDetailData.bannerPic
                            : images.DEFAULT_COVER
                    } }
                    style={ styles.bgImage } >
                    <View style={ styles.imageContainer }>
                        <Image
                            source={ {
                                uri: UserDetailData.profilePic !== null
                                    ? UserDetailData.profilePic
                                    : images.DEFAULT_PROFILE
                            } }
                            style={ styles.bgImage } />
                    </View>
                </ImageBackground>
            </View>

            <Tabs tabBarUnderlineStyle={ { backgroundColor: colors.primary } }>
                <Tab heading="PROFILE" tabStyle={ styles.tabs } textStyle={ styles.textTabs } activeTabStyle={ styles.activeTabs } activeTextStyle={ styles.activeTabs }>
                    <UserProfileDetail data={ data } onSend={ sendConnectionRequest } status={ ConnectionStatus } />
                </Tab>
                <Tab heading="CONNECTIONS" tabStyle={ styles.tabs } textStyle={ styles.textTabs } activeTabStyle={ styles.activeTabs } activeTextStyle={ styles.activeTabs }  >
                    <UserProfileConnection viewId={ props.route.params.id } />
                </Tab>
            </Tabs>


        </View >
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.base,
    },

    bgimageContainer: {
        height: '35%',
    },

    imageContainer: {
        height: 120,
        width: 120,
        borderRadius: Math.floor(Dimensions.get('window').width * 0.7 / 2),
        borderWidth: 2,
        borderColor: colors.base,
        overflow: 'hidden',
    },

    bgImage: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },

    tabs: {
        backgroundColor: colors.base,
    },

    textTabs: {
        color: colors.primary,
    },

    activeTabs: {
        color: colors.primary,
        backgroundColor: colors.base,
    },
});

const mapStateToProps = (state) => {
    return {
        Auths: state.AuthReducer.account
    }
}

export default connect(mapStateToProps)(UserProfile);