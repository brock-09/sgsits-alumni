import axios from 'axios';
import api from '../constants/api';

const axiosall = (method, url, params) => {

    const baseURL = api.BASE;

    if (method === 'get') {
        const getuser = axios.get(baseURL + url, params);
        return getuser;
    }
    else if (method === 'post') {
        const postuser = axios.post(baseURL + url, params);
        return postuser;
    }
    else if (method === 'delete') {
        const deleteuser = axios.delete(baseURL + url);
        return deleteuser;
    }
    else if (method === 'put') {
        const putuser = axios.put(baseURL + url, params);
        return putuser;
    }

}

export { axiosall };